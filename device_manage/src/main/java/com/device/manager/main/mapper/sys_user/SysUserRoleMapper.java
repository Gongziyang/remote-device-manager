package com.device.manager.main.mapper.sys_user;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Set;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/19 10:36
 * @file: SysUserRoleMapper
 * @description: 星期五
 */
@Mapper
public interface SysUserRoleMapper {

    @Select("select role_name from sys_role r ,sys_user_role ur,sys_user u where r.id = ur.role_id and ur.user_id = u.id and u.user_name = #{username}")
    Set<String> getRoleByUserName(@Param("username") String username);

    @Insert("insert into sys_user_role(user_id,role_id) values(#{userId},#{roleId})")
    int insert(@Param("userId") int userId, @Param("roleId") Integer roleId);
}
