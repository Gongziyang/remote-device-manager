package com.device.manager.main.vo.sys_user;

import java.io.Serializable;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/19 9:44
 * @file: SysRole
 * @description: 星期五
 */
public class SysRole implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private int id;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 描述
     */
    private String description;


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
