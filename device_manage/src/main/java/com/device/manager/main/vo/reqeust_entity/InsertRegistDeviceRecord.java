package com.device.manager.main.vo.reqeust_entity;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/9 17:19
 * @file: InsertRegistDeviceRecord
 * @description: 星期二
 */
public class InsertRegistDeviceRecord {

    private int operationId;

    private String userName;

    private String deviceNumber;

    private String deviceName;

    private int deviceType;

    private float deviceLng;

    private float deviceLat;

    private String deviceInfo;

    private int deviceStatus;

    private int farmId;

    private String registTime;

    public int getOperationId() {
        return operationId;
    }

    public void setOperationId(int operationId) {
        this.operationId = operationId;
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public int getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(int deviceType) {
        this.deviceType = deviceType;
    }

    public float getDeviceLng() {
        return deviceLng;
    }

    public void setDeviceLng(float deviceLng) {
        this.deviceLng = deviceLng;
    }

    public float getDeviceLat() {
        return deviceLat;
    }

    public void setDeviceLat(float deviceLat) {
        this.deviceLat = deviceLat;
    }

    public String getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(String deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public int getDeviceStatus() {
        return deviceStatus;
    }

    public void setDeviceStatus(int deviceStatus) {
        this.deviceStatus = deviceStatus;
    }

    public int getFarmId() {
        return farmId;
    }

    public void setFarmId(int farmId) {
        this.farmId = farmId;
    }

    public String getRegistTime() {
        return registTime;
    }

    public void setRegistTime(String registTime) {
        this.registTime = registTime;
    }
}
