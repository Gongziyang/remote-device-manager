package com.device.manager.main.mapper;

import com.device.manager.main.vo.entity.Farm;
import com.device.manager.main.vo.reqeust_entity.InsertRegistFarmRecord;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/8 23:32
 * @file: RegistFarmRecordMapper.xml
 * @description: 星期一
 */
@Mapper
public interface RegistFarmRecordMapper {

    public int insertRecord(InsertRegistFarmRecord registFarmRecord);

    //通过operationId和UserId来从注册农场表中获取用户对应的注册信息
    public Farm getFarmByOptionIdAndUserName(Map<String, Object> map);

    //管理员审核成功之后，将该条记录删除
    public int deleteByOptionIdAndUserName(Map<String, Object> map);
}
