package com.device.manager.main.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.device.manager.main.mapper.DeviceMapper;
import com.device.manager.main.service.*;
import com.device.manager.main.utils.custom.ResponseUtils;
import com.device.manager.main.vo.entity.Device;
import com.device.manager.main.vo.msg_standard.TResponse;
import com.device.manager.main.vo.reqeust_entity.*;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/9 14:03
 * @file: DeviceServiceImpl
 * @description: 星期二
 */
@Service
public class DeviceServiceImpl implements DeviceService {

    @Autowired
    DeviceMapper deviceMapper;

    @Autowired
    UserOperationService userOperationService;

    @Autowired
    RegistDeviceRecordService deviceRecordService;

    @Autowired
    CheckApplyService checkApplyService;


    @Override
    public int itemSize(Map<String, Object> map) {
        return deviceMapper.itemSize(map);
    }


    /**
     * 删除农场的时候需要删除设备表里面的设备信息，此方法供删除农场时调用
     **/
    @Override
    public int deleteDevicesByFarmId(int farmId) {
        //返回实际操作的条目数
        return deviceMapper.deleteDevicesByFarmId(farmId);
    }

    /**
     * 批量删除农场时需要删除农场关联的设备，该方法可提供批量删除农场时调用
     **/
    @Override
    public int deleteDevicesByFarmIds(int[] farmIds) {
        Map<String, Object> ids = new HashMap<>(16);
        ids.put("ids", farmIds);
        return deviceMapper.deleteDevicesByFarmIds(ids);
    }

    @Override
    public TResponse<BaseResp> getDevicesByPageHelper(QueryDeviceReq req) {
        int pageNo = req.getPageNo();
        int pageSize = req.getPageSize();
        //使用分页插件进行分页查询
        PageHelper.startPage(pageNo, pageSize);
        TResponse<BaseResp> response = new TResponse<>();
        //可根据条件筛选
        List<Device> devicesByPageHelper = deviceMapper.getDevicesByPageHelper(req);
        System.out.println(devicesByPageHelper);
        Map<String, Object> map = new HashMap<>(16);
        map.put("userName", req.getUserName());
        map.put("farmId", req.getFarmId());
        map.put("deviceName", req.getDeviceName());
        map.put("deviceNumber", req.getDeviceNumber());
        int itemSize = itemSize(map);
        System.out.println("[req-itemSize]"+JSONObject.toJSONString(map));
        System.out.println("[size]"+itemSize);
        int pageCount = itemSize % pageSize == 0 ? itemSize / pageSize : (itemSize / pageSize + 1);
        if (pageNo > pageCount) {
            ResponseUtils.set(response, -1, "暂无更多的信息", null);
        } else {
            BaseResp deviceResp = new BaseResp();
            deviceResp.setItemSize(itemSize);
            deviceResp.setPageCount(pageCount);
            deviceResp.setPageNo(pageNo);
            deviceResp.setList(devicesByPageHelper);
            ResponseUtils.set(response, 0, "共查询到：" + itemSize + "条记录", deviceResp);
        }
        String timeStamp = getTimestamp();
        response.setTimeStamp(timeStamp);
        return response;
    }

    @Override
    public TResponse<Integer> updateDevice(Device device) {

        TResponse<Integer> response = new TResponse<>();
        String timeStamp = getTimestamp();
        int affcts = deviceMapper.updateDevice(device);
        //更新失败
        if (affcts == 0) {
            ResponseUtils.set(response, -1, "编辑失败", affcts);
        } else {
            ResponseUtils.set(response, 0, "编辑成功", affcts);
        }
        response.setTimeStamp(timeStamp);
        return response;
    }

    /**
     * 注册设备
     * 1、先向用户操作记录表中插入数据，并返回操作记录ID
     * 2、先用户注册设备记录表中插入数据
     * 3、向管理员审核用户注册表中插入数据
     **/
    @Override
    public TResponse<Integer>   registDevice(RegistDeviceReq deviceReq) {

        //1、向用户操作记录表中插入数据
        InsertOperation operation = new InsertOperation();
        operation.setUserName(deviceReq.getUserName());
        operation.setOperationInfo(JSONObject.toJSONString(deviceReq));
        operation.setOperationType(deviceReq.getType());
        operation.setResult(1);
        operation.setSubmitTime(deviceReq.getRegistTime());
        int operationId = userOperationService.insertOperation(operation);
        //2、向用户注册设备记录表中插入数据
        InsertRegistDeviceRecord deviceRecord = new InsertRegistDeviceRecord();
        deviceRecord.setUserName(deviceReq.getUserName());
        deviceRecord.setDeviceInfo(deviceReq.getDeviceInfo());
        deviceRecord.setDeviceLat(deviceReq.getDeviceLat());
        deviceRecord.setDeviceLng(deviceReq.getDeviceLng());
        deviceRecord.setDeviceName(deviceReq.getDeviceName());
        deviceRecord.setDeviceType(deviceReq.getDeviceType());
        deviceRecord.setOperationId(operationId);
        deviceRecord.setDeviceNumber(deviceReq.getDeviceNumber());
        deviceRecord.setFarmId(deviceReq.getFarmId());
        deviceRecord.setRegistTime(deviceReq.getRegistTime());
        int affects0 = deviceRecordService.insertRecord(deviceRecord);
        //3、向管理员审核注册表中插入相关数据
        InsertCheckApply insertCheckApply = new InsertCheckApply();
        insertCheckApply.setUserName(deviceReq.getUserName());
        insertCheckApply.setApplyTime(deviceReq.getRegistTime());
        insertCheckApply.setApplyType(deviceReq.getType());
        insertCheckApply.setApplyInfo("注册设备");
        //1代表正在审核中
        insertCheckApply.setStatus(1);
        insertCheckApply.setOperationId(operationId);
        int affcts1 = checkApplyService.insertApply(insertCheckApply);

        //封装返回数据
        TResponse<Integer> response = new TResponse<>();
        String timeStamp = getTimestamp();
        if (affects0 + affcts1 <= 1) {
            ResponseUtils.set(response, -1, "操作失败", -1);
        } else {
            ResponseUtils.set(response, 0, "请求已提交，请等待管理员处理", affects0 + affcts1);
        }
        response.setTimeStamp(timeStamp);
        return response;
    }

    /**
     * 1、通过id删除设备
     **/
    @Override
    public TResponse<Integer> deleteDeviceById(int id) {
        TResponse<Integer> response = new TResponse<>();
        String timeStamp = getTimestamp();
        int affects0 = deviceMapper.deleteDeviceById(id);
        if (affects0 <= 0) {
            ResponseUtils.set(response, -1, "操作失败", -1);
        } else {
            ResponseUtils.set(response, 0, "操作成功，共删除：" + affects0 + "条记录", affects0);
        }
        response.setTimeStamp(timeStamp);
        return response;
    }

    /**
     * 1、根据id删除设备
     **/
    @Override
    public TResponse<Integer> deleteDeviceByIds(int[] ids) {
        Map<String, Object> idsMap = new HashMap<>(16);
        idsMap.put("ids", ids);
        TResponse<Integer> response = new TResponse<>();
        String timeStamp = getTimestamp();
        int affects0 = deviceMapper.deleteDevicesByIds(idsMap);
        if (affects0 <= 0) {
            ResponseUtils.set(response, -1, "操作失败", -1);
        } else {
            ResponseUtils.set(response, 0, "操作成功，共删除：" + affects0 + "条记录", affects0);
        }
        response.setTimeStamp(timeStamp);
        return response;
    }

    @Override
    public int deleteDevicesByUserName(String userName) {
        return deviceMapper.deleteDevicesByUserName(userName);
    }

    @Override
    public int deleteDevicesByUserNames(String[] userNames) {
        Map<String, Object> map = new HashMap<>(100);
        map.put("userNames", userNames);
        return deviceMapper.deleteDevicesByUserNames(map);
    }


    @Override
    public int insertDevice(Device device) {
        return deviceMapper.insertDevice(device);
    }

    @Override
    public int updateState(int deviceId, int status) {
        Map<String, Object> map = new HashMap<>();
        map.put("status", status);
        map.put("deviceId", deviceId);
        return deviceMapper.updateState(map);
    }

    @Override
    public int updateLoginTime(int deviceId, String loginTime) {
        Map<String, Object> map = new HashMap<>();
        map.put("deviceId", deviceId);
        map.put("loginTime", loginTime);
        return deviceMapper.updateLoginTime(map);
    }

    @Override
    public List<Integer> getDeviceIdsByUserName(String userName) {
        return deviceMapper.getDeviceIdsByUserName(userName);
    }

    @Override
    public List<Integer> getDeviceIds() {
        return deviceMapper.getDeviceIds();
    }

    private String getTimestamp() {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
        String timeStamp = format.format(System.currentTimeMillis());
        return timeStamp;
    }
}
