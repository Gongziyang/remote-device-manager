package com.device.manager.main.service.sys_user;

import com.device.manager.main.vo.sys_user.SysMenu;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/21 9:14
 * @file: SysPermissionService
 * @description: 星期日
 */
public interface SysMenuService {

    List<SysMenu> queryByUser(String username);
}
