package com.device.manager.main.netty;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;

import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Socket;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/17 13:39
 * @file: PackageSpilter
 * @description: 星期三
 * <p>
 * 该类要调用完指定的长度后才会继续调用下面的handler
 */
public class PackageSpilter extends LengthFieldBasedFrameDecoder {


    public PackageSpilter() {
        super(Integer.MAX_VALUE, 2,
                2, -4,
                0, true);
    }

    @Override
    protected Object decode(ChannelHandlerContext ctx, ByteBuf in) throws Exception {

        //如果报文头部长度小于15
        if (in.readableBytes() < Constant.HEADER_SIZE) {
            throw new Exception("错误的报文格式");
        }


        return super.decode(ctx, in);
    }
}
