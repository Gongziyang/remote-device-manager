package com.device.manager.main.service.impl;

import com.device.manager.main.mapper.UserOperationMapper;
import com.device.manager.main.service.UserOperationService;
import com.device.manager.main.utils.custom.ResponseUtils;
import com.device.manager.main.vo.entity.UserOperation;
import com.device.manager.main.vo.msg_standard.TResponse;
import com.device.manager.main.vo.reqeust_entity.BaseResp;
import com.device.manager.main.vo.reqeust_entity.InsertOperation;
import com.device.manager.main.vo.reqeust_entity.QueryOperationReq;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/8 23:41
 * @file: UserOperationServiceImpl
 * @description: 星期一
 */
@Service
public class UserOperationServiceImpl implements UserOperationService {

    @Autowired
    UserOperationMapper userOperationMapper;

    @Override
    public int insertOperation(InsertOperation operation) {
        return userOperationMapper.insertOperation(operation);
    }

    @Override
    public TResponse<BaseResp> queryOperationByPageHelper(QueryOperationReq operationReq) {
        int pageNo = operationReq.getPageNo();
        int pageSize = operationReq.getPageSize();
        TResponse<BaseResp> response = new TResponse<>();
        PageHelper.startPage(pageNo, pageSize);
        List<UserOperation> userOperations = userOperationMapper.queryOperationByPageHelper(operationReq);
        int itemSize = userOperationMapper.itemSize(operationReq.getUserName());
        int pageCount = itemSize % pageSize == 0 ? itemSize / pageSize : (itemSize / pageSize + 1);
        if (userOperations == null || userOperations.size() == 0) {
            ResponseUtils.set(response, -1, "暂无更多信息", null);
        } else {

            BaseResp operationResp = new BaseResp();
            operationResp.setItemSize(itemSize);
            operationResp.setPageCount(pageCount);
            operationResp.setList(userOperations);
            operationResp.setPageNo(pageNo);
            ResponseUtils.set(response, 0, "查询成功", operationResp);
        }
        response.setTimeStamp(getTimeStamp());
        return response;
    }

    @Override
    public int itemSize(String userId) {
        return userOperationMapper.itemSize(userId);
    }

    @Override
    public TResponse<Integer> deleteById(int id) {
        TResponse<Integer> response = new TResponse<>();
        int affcts = userOperationMapper.deleteById(id);
        if (affcts == 0) {
            ResponseUtils.set(response, -1, "删除失败", -1);
        } else {
            ResponseUtils.set(response, 0, "成功", affcts);
        }
        response.setTimeStamp(getTimeStamp());
        return response;
    }

    @Override
    public TResponse<Integer> deleteByIds(int[] ids) {
        TResponse<Integer> response = new TResponse<>();
        Map<String, Object> idsMap = new HashMap<>(16);
        idsMap.put("ids", ids);
        int affects = userOperationMapper.deleteByIds(idsMap);
        if (affects <= 0) {
            ResponseUtils.set(response, -1, "批量删除失败", -1);
        } else {
            ResponseUtils.set(response, 0, "成功", affects);
        }
        response.setTimeStamp(getTimeStamp());
        return response;
    }

    @Override
    public int updateStatus(String userName, int operationId, int status) {
        Map<String, Object> map = new HashMap<>(16);
        map.put("userName", userName);
        map.put("operationId", operationId);
        map.put("status", status);
        return userOperationMapper.updateStatus(map);
    }


    private String getTimeStamp() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String timeStamp = dateFormat.format(System.currentTimeMillis());
        return timeStamp;
    }
}
