package com.device.manager.main.redis;

import com.device.manager.main.redis.MessageConstant;
import com.device.manager.main.redis.MessageReciverSupport;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;

import java.util.concurrent.CountDownLatch;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/15 22:48
 * @file: RedisPubSubConfig
 * @description: 星期一
 */
@Configuration
@AutoConfigureAfter(MessageReciverSupport.class)
public class RedisPubSubConfig {

    /**
     * @param connectionFactory 链接工厂
     * @param listener          ,实现MessageListener接口 消息监听者适配器
     * @return redis消息监听容器
     * description: 注入redis的消息监听容器，注入消息监听适配器和连接工厂，可以注册多个消息监听适配器
     */
    @Bean
    public RedisMessageListenerContainer container(RedisConnectionFactory connectionFactory,
                                                   MessageReciverSupport listener) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        //可以添加多个 messageListener
        //注册消息监听者适配器以及其订阅的主题，一旦有消息发布到对应主题，会通知监听者
        container.addMessageListener(listener, new PatternTopic(MessageConstant.TOPIC));
        return container;
    }

    @Bean
    public MessageReciverSupport listener() {
        return new MessageReciverSupport(latch());
    }

    @Bean
    public CountDownLatch latch() {
        return new CountDownLatch(1);
    }
}
