package com.device.manager.main.redis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/15 22:55
 * @file: RedisService
 * @description: 星期一
 */
@Service
public class RedisService {

    //注入redisTemplate
    @Autowired
    RedisTemplate<String, String> redisTemplate;

    Logger logger = LoggerFactory.getLogger(RedisService.class);

    public String sendMessage(String message) {
        try {
            redisTemplate.convertAndSend(MessageConstant.TOPIC, message);
            logger.info("[RedisService]发布了一条消息:{}", message);
            return "发送成功";
        } catch (Exception e) {
            e.printStackTrace();
            return "发送失败";
        }
    }
}
