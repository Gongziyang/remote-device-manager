package com.device.manager.main.netty.client;

import com.alibaba.fastjson.JSONObject;
import com.device.manager.main.netty.Decoder;
import com.device.manager.main.netty.Encoder;
import com.device.manager.main.netty.PackageSpilter;
import com.device.manager.main.service.DeviceService;
import com.device.manager.main.utils.custom.BeanUtils;
import com.device.manager.main.vo.entity.Device;
import com.device.manager.main.vo.msg_standard.TResponse;
import com.device.manager.main.vo.reqeust_entity.BaseResp;
import com.device.manager.main.vo.reqeust_entity.QueryDeviceReq;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.net.InetSocketAddress;
import java.util.*;
import java.util.concurrent.ScheduledExecutorService;


/**
 * @author: yyf
 * @create: 2021 03 2021/3/15 23:37
 * @file: NettyClient
 * @description: 星期一
 */
public class Client {

    private BizHandler bizHandler = new BizHandler();

    DeviceService deviceService = BeanUtils.getBean(DeviceService.class);

    /**
     * 设备ID和信道的映射
     **/
    private Map<String, ChannelFuture> futures;

    {
      sendWarning();
    }

    public void connect() {

        Bootstrap bootstrap = new Bootstrap();
        EventLoopGroup worker = new NioEventLoopGroup();
        bootstrap.group(worker);
        bootstrap.channel(NioSocketChannel.class);
        bootstrap.handler(new ChannelInitializer<SocketChannel>() {
            @Override
            protected void initChannel(SocketChannel socketChannel) throws Exception {
//              socketChannel.pipeline().addLast(new IdleStateHandler(60, 0, 0, TimeUnit.SECONDS));
                socketChannel.pipeline().addLast(new PackageSpilter());
                socketChannel.pipeline().addLast(new Encoder());
                socketChannel.pipeline().addLast(new Decoder());
                socketChannel.pipeline().addLast(new ClientHandler());
            }
        });
        bootstrap.option(ChannelOption.SO_KEEPALIVE, true);
        try {
            List<Device> deviceIds = getDeviceIds();
            System.out.println("[设备总量]" + deviceIds.size());
            futures = new HashMap<>(deviceIds.size());
            Iterator<Device> iterator = deviceIds.iterator();
            while (iterator.hasNext()) {
                Device device = iterator.next();
                ChannelFuture future = bootstrap.connect(new InetSocketAddress("localhost", 8888)).sync();
                futures.put(device.getDeviceId() + "", future);
                bizHandler.sendLogin(future.channel(), device);
            }
            for (Device device : deviceIds) {
                ChannelFuture channelFuture = futures.get(device.getDeviceId()+"");
                //优雅地关闭
                channelFuture.channel().closeFuture().sync();
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            worker.shutdownGracefully();
        }
    }

    public List<Device> getDeviceIds() {
        QueryDeviceReq req = new QueryDeviceReq();
        req.setPageNo(1);
        req.setPageSize(1000);
        TResponse<BaseResp> response = deviceService.getDevicesByPageHelper(req);
        List<Device> devices = (List<Device>) response.getData().getList();
        return devices;
    }

    public void sendWarning() {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                List<Device> deviceIds = getDeviceIds();
                for (Device device : deviceIds) {
                    ChannelFuture channelFuture = futures.get(device.getDeviceId() + "");
                    bizHandler.sendWarning(channelFuture.channel(), device);
                }
            }
        }, 15000, 150000);
    }
}
