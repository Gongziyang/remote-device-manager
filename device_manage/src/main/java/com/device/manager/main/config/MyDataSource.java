package com.device.manager.main.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import javax.sql.DataSource;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/8 16:51
 * @file: MyDataSource
 * @description: 星期一
 * 数据源配置类
 */
@Configuration
public class MyDataSource {

    @Value("${spring.datasource.url}")
    private String url;
    @Value("${spring.datasource.driver-class-name}")
    private String driverClassName;
    @Value("${spring.datasource.username}")
    private String username;
    @Value("${spring.datasource.password}")
    private String password;

    //使用德鲁伊数据源
    public DataSource dataSource() {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setUrl(url);
        dataSource.setDriverClassName(driverClassName);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        return dataSource;
    }


    //配置sqlSession工厂
    @Bean
    public SqlSessionFactory sqlSessionFactory() throws Exception {
        SqlSessionFactoryBean factory = new SqlSessionFactoryBean();
        factory.setDataSource(dataSource());

        //这里可以在application.properties中配置
        //获取mapper.xml文件资源
        Resource[] mappers = new PathMatchingResourcePatternResolver()
                .getResources("classpath:mapper/**/*.xml");
        //设置mybatis的mapper文件的映射路径
        factory.setMapperLocations(mappers);
        Resource config = new PathMatchingResourcePatternResolver().
                getResource("classpath:mybatis-config.xml");
        factory.setConfigLocation(config);

        //从工厂中拿到一个实例
        return factory.getObject();
    }
}
