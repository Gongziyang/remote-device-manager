package com.device.manager.main.vo.reqeust_entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/8 23:24
 * @file: InsertFarmReq
 * @description: 星期一
 * <p>
 * 编辑农场和插入农场都使用该类
 */
@ApiModel(description = "包含注册农场时所需要的参数")
public class RegistFarmReq {

    @ApiModelProperty(name = "userName", value = "用户名", dataType = "String", example = "yyf")
    private String userName;
    @ApiModelProperty(name = "type", value = "注册类型[1 注册农场 2注册设备]", dataType = "Integer", example = "1")
    private int type;  //注册类型
    @ApiModelProperty(name = "farmType", value = "农场类型[1 养殖场 2 其他]", dataType = "Integer", example = "1")
    private int farmType;
    @ApiModelProperty(name = "lng", value = "农场经度", dataType = "float", example = "32.2")
    private float lng;
    @ApiModelProperty(name = "lat", value = "农场纬度", dataType = "float", example = "33.1")
    private float lat;
    @ApiModelProperty(name = "farmInfo", value = "农场信息", dataType = "String", example = "该农场位于...")
    private String farmInfo;
    @ApiModelProperty(name = "farmStatus", value = "农场状态", dataType = "Integer", example = "0")
    private int farmStatus;
    @ApiModelProperty(name = "registTime", value = "注册时间", dataType = "String", example = "2020-09-01")
    private String registTime;
    @ApiModelProperty(name = "farmNumber", value = "农场编号", dataType = "String", example = "001")
    private String farmNumber;
    @ApiModelProperty(name = "farmName", value = "农场名称", dataType = "String", example = "十陵养猪场")
    private String farmName;

    public String getUserName() {
        return userName;
    }


    public void setUserName(String userName) {
        this.userName = userName;
    }


    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getFarmType() {
        return farmType;
    }

    public void setFarmType(int farmType) {
        this.farmType = farmType;
    }

    public float getLng() {
        return lng;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public String getFarmInfo() {
        return farmInfo;
    }

    public void setFarmInfo(String farmInfo) {
        this.farmInfo = farmInfo;
    }

    public int getFarmStatus() {
        return farmStatus;
    }

    public void setFarmStatus(int farmStatus) {
        this.farmStatus = farmStatus;
    }

    public String getRegistTime() {
        return registTime;
    }

    public void setRegistTime(String registTime) {
        this.registTime = registTime;
    }

    public String getFarmNumber() {
        return farmNumber;
    }

    public void setFarmNumber(String farmNumber) {
        this.farmNumber = farmNumber;
    }

    public String getFarmName() {
        return farmName;
    }

    public void setFarmName(String farmName) {
        this.farmName = farmName;
    }
}
