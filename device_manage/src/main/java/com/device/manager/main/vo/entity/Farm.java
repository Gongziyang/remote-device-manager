package com.device.manager.main.vo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/8 16:18
 * @file: Farm
 * @description: 星期一
 */
public class Farm {

    //农场ID，必填字段
    private int farmId;
    private String farmNumber; //农场编号，必填字段
    private String farmName; //农场名称，必填字段
    private int farmType;  //农场类型，必填字段
    private float farmLng; //农场纬度，必填字段
    private float farmLat; //农场经度，必填字段
    private int farmStatus; //农场状态，必填字段
    private String farmInfo; //农场说明
    private String registTime;
    private String userName;

    public int getFarmId() {
        return farmId;
    }

    public void setFarmId(int farmId) {
        this.farmId = farmId;
    }

    public String getFarmNumber() {
        return farmNumber;
    }

    public void setFarmNumber(String farmNumber) {
        this.farmNumber = farmNumber;
    }

    public String getFarmName() {
        return farmName;
    }

    public void setFarmName(String farmName) {
        this.farmName = farmName;
    }

    public int getFarmType() {
        return farmType;
    }

    public void setFarmType(int farmType) {
        this.farmType = farmType;
    }

    public float getFarmLng() {
        return farmLng;
    }

    public void setFarmLng(float farmLng) {
        this.farmLng = farmLng;
    }

    public float getFarmLat() {
        return farmLat;
    }

    public void setFarmLat(float farmLat) {
        this.farmLat = farmLat;
    }

    public int getFarmStatus() {
        return farmStatus;
    }

    public void setFarmStatus(int farmStatus) {
        this.farmStatus = farmStatus;
    }

    public String getFarmInfo() {
        return farmInfo;
    }

    public void setFarmInfo(String farmInfo) {
        this.farmInfo = farmInfo;
    }

    public String getRegistTime() {
        return registTime;
    }

    public void setRegistTime(String registTime) {
        this.registTime = registTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
