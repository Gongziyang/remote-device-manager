package com.device.manager.main.redis;

import com.alibaba.fastjson.JSONObject;
import com.device.manager.main.netty.msg.WarningMsg;
import com.device.manager.main.service.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/15 22:51
 * @file: MessageReciverSupport
 * @description: 星期一
 */
public class MessageReciverSupport implements MessageListener {


    public CountDownLatch latch;

    public MessageReciverSupport(CountDownLatch latch) {
        this.latch = latch;
    }


    public Map<String, WarningMsg> warningMsgMap = new HashMap<>(1000);

    @Override
    public void onMessage(Message message, byte[] bytes) {

        byte[] body = message.getBody();
        String bodyStr = new String(body);
        System.out.println("[收到消息]\t" + bodyStr);
        JSONObject jsonObject = JSONObject.parseObject(bodyStr);
        WarningMsg warningMsg = jsonObject.toJavaObject(WarningMsg.class);
        warningMsgMap.put(warningMsg.getDeviceId() + "", warningMsg);
        System.out.println("收到一条报警消息：" + bodyStr);
        latch.countDown();
    }

    public WarningMsg getWarnByUserName(List<Integer> deviceIds) {
        Iterator<Integer> iterator = deviceIds.iterator();
        Integer deviceId;
        WarningMsg warningMsg = null;
        while (iterator.hasNext()) {
            deviceId = iterator.next();
            warningMsg = warningMsgMap.get(deviceId + "");
            if (warningMsg != null) {
                warningMsgMap.remove(deviceId + "");
                break;
            }
        }
        return warningMsg;
    }
}
