package com.device.manager.main.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.device.manager.main.constant.CommonConstant;
import com.device.manager.main.mapper.UserMapper;
import com.device.manager.main.mapper.sys_user.SysUserRoleMapper;
import com.device.manager.main.service.DeviceService;
import com.device.manager.main.service.FarmService;
import com.device.manager.main.service.UserService;
import com.device.manager.main.service.sys_user.SysPermissionService;
import com.device.manager.main.utils.custom.ResponseUtils;
import com.device.manager.main.vo.msg_standard.TResponse;
import com.device.manager.main.vo.reqeust_entity.BaseResp;
import com.device.manager.main.vo.reqeust_entity.QueryUserReq;
import com.device.manager.main.vo.sys_user.SysUser;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/11 15:37
 * @file: UserServiceImpl
 * @description: 星期四
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserMapper userMapper;

    @Autowired
    FarmService farmService;

    @Autowired
    DeviceService deviceService;

    @Autowired
    private SysPermissionService permissionService;

    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;

    Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Override
    public int itemSize() {
        return userMapper.itemSize();
    }

    @Override
    public TResponse<BaseResp> getUserByPageHelper(QueryUserReq req) {
        //设置分页查询
        PageHelper.startPage(req.getPageNo(), req.getPageSize());
        TResponse<BaseResp> response = new TResponse<>();
        BaseResp userResp = new BaseResp();
        List<SysUser> users = userMapper.getUserByPageHelper(req);
        int itemSize = itemSize();
        int pageCount = itemSize % req.getPageSize() == 0 ? itemSize / req.getPageSize() : (itemSize / req.getPageSize() + 1);
        if (req.getPageNo() > pageCount) {
            ResponseUtils.set(response, -1, "暂无更多信息", null);
        } else {

            userResp.setItemSize(itemSize);
            userResp.setPageCount(pageCount);
            userResp.setPageNo(req.getPageNo());
            userResp.setList(users);
            ResponseUtils.set(response, 0, "查询成功，共：" + itemSize + "条记录", userResp);
        }
        response.setTimeStamp(getTimestamp());
        return response;
    }

    /**
     * 1、从用户表中删除该用户
     * 2、删除该用户相关的设备和农场，先删除设备，再删除农场
     **/
    @Override
    public TResponse<Integer> deleteUserByUserName(String userName) {

        int affects2 = deviceService.deleteDevicesByUserName(userName);
        int affects1 = farmService.deleteFarmByUserName(userName);
        int affects0 = userMapper.deleteUserById(userName);
        int total = affects0 + affects1 - affects2;
        TResponse<Integer> response = new TResponse<>();

        if (total <= 4) {
            ResponseUtils.set(response, -1, "操作异常", -1);
        } else {
            ResponseUtils.set(response, 0, "删除成功，共删除：" + total + "条记录", total);
        }
        response.setTimeStamp(getTimestamp());
        return response;
    }

    @Override
    public TResponse<Integer> deleteUserByUserNames(String[] userNames) {
        Map<String, Object> userNamesMap = new HashMap<>();
        userNamesMap.put("ids", userNames);
        int affects4 = deviceService.deleteDevicesByUserNames(userNames);
        int affects1 = farmService.deleteFarmByUserNames(userNames);
        int affects0 = userMapper.deleteUserByIds(userNamesMap);
        int total = affects0 + affects1 + affects4;
        TResponse<Integer> response = new TResponse<>();

        if (total <= 4) {
            ResponseUtils.set(response, -1, "操作异常", -1);
        } else {
            ResponseUtils.set(response, 0, "删除成功，共删除：" + total + "条记录", total);
        }
        response.setTimeStamp(getTimestamp());
        return response;
    }

    @Override
    public TResponse<Integer> updateUser(SysUser user) {
        TResponse<Integer> response = new TResponse<>();
        int affects = userMapper.updateUser(user);
        if (affects == 0) {
            ResponseUtils.set(response, -1, "操作失败", -1);
        } else {
            ResponseUtils.set(response, 0, "操作成功", affects);
        }
        response.setTimeStamp(getTimestamp());
        return response;
    }

    private String getTimestamp() {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
        String timeStamp = format.format(System.currentTimeMillis());
        return timeStamp;
    }


    @Override
    public SysUser getUserByName(String username) {
        return userMapper.getUserByName(username);
    }

    /**
     * 通过用户名获取用户角色集合
     *
     * @param username 用户名
     * @return 角色集合
     */
    @Override
//    @Cacheable(value = CommonConstant.LOGIN_USER_RULES_CACHE, key = "'Roles_'+#username")
    public Set<String> getUserRolesSet(String username) {
        // 查询用户拥有的角色集合
        Set<String> roles = sysUserRoleMapper.getRoleByUserName(username);
        System.out.println("[roles]" + JSONObject.toJSONString(roles));
        logger.info("-------通过数据库读取用户拥有的角色Rules------username： " + username + ",Roles size: " + (roles == null ? 0 : roles.size()));
        return roles;
    }

    /**
     * 通过用户名获取用户权限集合
     *
     * @param username 用户名
     * @return 权限集合
     */
    @Override
//    @Cacheable(value = CommonConstant.LOGIN_USER_RULES_CACHE, key = "'Permissions_'+#username")
    public Set<String> getUserPermissionsSet(String username) {
        Set<String> permissions = permissionService.getByUser(username);
        logger.info("-------通过数据库读取用户拥有的权限Perms------username： " + username + ",Perms size: " + (permissions == null ? 0 : permissions.size()));
        return permissions;
    }

    @Override
    public int regist(SysUser user) {
        return userMapper.regist(user);
    }

    /**
     * 校验用户是否有效
     *
     * @param sysUser
     * @return
     */
    @Override
    public TResponse<Object> checkUserIsEffective(SysUser sysUser) {
        TResponse<Object> response = new TResponse<>();
        //情况1：根据用户信息查询，该用户不存在
        if (sysUser == null) {
            response.setMsg("用户不存在，请重新注册");
            response.setRet(-1);
            return response;
        }

        //情况3：根据用户信息查询，该用户已被冻结
        if (CommonConstant.USER_FREEZE.equals(sysUser.getFrezzeState())) {
            response.setMsg("异常！该账号已被冻结,请联系管理员");
            response.setRet(-1);
            return response;
        }
        return response;
    }
}
