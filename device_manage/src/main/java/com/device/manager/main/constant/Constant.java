package com.device.manager.main.constant;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/12 11:18
 * @file: Constant
 * @description: 星期五
 */
public class Constant {
    //查询成功返回状态码
    public static final int Query_SUCCESS = 0;
    //查询失败返回状态码
    public static final int Query_FAILED = -1;
    //最后一页返回状态码
    public static final int NO_MORE_INFO = -2;
    //注册农场时，类型为1
    public static final int REGIST_FARM = 1;
    //注册设备时为2
    public static final int REGIST_DEVICE = 0;


    public static String OPT_FAILED = "操作失败";


    //管理员更新农场信息        --已测试
    public static final String ADMIN_UPDATE_FARM_INFO = "/admin/update/update_farm_info";
    //管理员查询用户信息        --已测试
    public static final String ADMIN_QUERY_USERS_INFO = "/admin/query/query_users_info";
    //管理员删除用户信息        --已测试
    public static final String ADMIN_DELETE_USER_INFO = "/admin/update/delete_user_info";
    //管理员批量删除用户信息     --已测试
    public static final String ADMIN_DELETE_USERS_INFO = "/admin/update/delete_users_info";
    //管理员修改用户信息        --已测试
    public static final String ADMIN_UPDATE_USER_INFO = "/admin/update/update_user_info";
    //管理员查看用户申请信息     --已测试
    public static final String ADMIN_QUERY_APPLICATIONS_INFO = "/admin/query/query_applications_info";
    //管理员删除申请信息        --已测试
    public static final String ADMIN_DELETE_APPLY_INFO = "/admin/update/delete_apply_info";
    //管理员批量删除申请信息     --已测试
    public static final String ADMIN_DELETE_APPLICATIONS_INFO = "/admin/update/delete_applications_info";
    //管理员同意用户申请        --已测试
    public static final String ADMIN_AGREE_APPLY_INFO = "/admin/check/agree_apply_info";
    //管理员拒绝用户申请        --已测试
    public static final String ADMIN_REFUSED_APPLY_INFO = "/admin/check/refused_apply_info";

    //用户查询农场信息         --已测试
    public static final String USER_QUERY_FARMS_INFO = "/user/query/query_farms_info";
    //用户删除农场信息         --已测试
    public static final String USER_DELETE_FARM_INFO = "/user/update/delete_farm_info";
    public static final String USER_DELETE_FARMS_INFO = "/user/update/delete_farms_info";
    //用户注册农场信息         --已测试
    public static final String USER_REGIST_FARM_INFO = "/user/update/regist_farm_info";
    //用户更新农场信息         --已测试
    public static final String USER_UPDATE_FARM_INFO = "/user/update/update_farm_info";
    //用户查询设备信息         --已测试
    public static final String USER_QUERY_DEVICES_INFO = "/user/query/query_devices_info";
    //用户删除设备信息         --已测试
    public static final String USER_DELETE_DEVICE_INFO = "/user/update/delete_device_info";
    public static final String USER_DELETE_DEVICES_INFO = "/user/update/delete_devices_info";
    //用户注册设备信息         --已测试
    public static final String USER_REGIST_DEVICE_INFO = "/user/update/regist_device_info";
    //用户更新设备信息         --已测试
    public static final String USER_UPDATE_DEVICE_INFO = "/user/update/update_device_info";
    //用户查询操作记录         --已测试
    public static final String USER_QUERY_OPERATIONS_INFO = "/user/query/query_operations_info";
    //用户删除操作记录         --已测试
    public static final String USER_DELETE_OPERATION_INFO = "/user/update/delete_operation_info";
    //用户批量删除操作记录      --已测试
    public static final String USER_DELETE_OPERATIONS_INFO = "/user/update/delete_operations_info";
    //用户查询报警记录         --已测试
    public static final String USER_QUERY_WARNINGS_INFO = "/user/query/query_warnings_info";
    //用户删除报警记录         --已测试
    public static final String USER_DELETE_WARNING_INFO = "/user/update/delete_warning_info";
    //用户批量删除报警记录      --已测试
    public static final String USER_DELETE_WARNINGS_INFO = "/user/update/delete_warnings_info";


}
