package com.device.manager.main.netty.msg;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.netty.channel.Channel;

import java.io.Serializable;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/16 17:09
 * @file: BaseInf
 * @description: 星期二
 */
public class BaseInf<Object> implements Serializable {


    /**
     * 版本号：short 2个字节
     **/
    public short version;

    /**
     * 包长度  short  2个字节
     **/
    public short length;

    /**
     * 报文序号  int  4个字节
     * 登陆报文 1xxxx
     * 报警报文 2xxxx
     * 控制报文 3xxxx
     * 响应报文的seq和收到的报文序列号一致
     **/
    public int seq;

    /**
     * 类型  0 平台->设备  1 设备->平台 2个字节
     **/
    public short type;

    /**
     * byte cmd:命令号  1个字节
     * 1：设备登录请求
     * 2：设备警报
     * 3：控制设备
     * 4：返回报文
     * 5：心跳报文
     **/
    public byte cmd;

    /**
     * 设备id 4个字节
     **/
    public int deviceId;

    /**
     * 报文消息体
     **/
    public Object msg;


    /**
     * CRC循环冗余校验码 2 个字节
     **/
    public short crc;


    public byte getCmd() {
        return cmd;
    }

    public void setCmd(byte cmd) {
        this.cmd = cmd;
    }

    public Object getMsg() {
        return msg;
    }

    public void setMsg(Object msg) {
        this.msg = msg;
    }

    public short getVersion() {
        return version;
    }

    public void setVersion(short version) {
        this.version = version;
    }

    public short getLength() {
        return length;
    }

    public void setLength(short length) {
        this.length = length;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public short getType() {
        return type;
    }

    public void setType(short type) {
        this.type = type;
    }

    public int getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }

    public short getCrc() {
        return crc;
    }

    public void setCrc(short crc) {
        this.crc = crc;
    }
}
