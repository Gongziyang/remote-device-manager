package com.device.manager.main.service;

import com.device.manager.main.vo.entity.Farm;
import com.device.manager.main.vo.msg_standard.TResponse;
import com.device.manager.main.vo.reqeust_entity.BaseResp;
import com.device.manager.main.vo.reqeust_entity.RegistFarmReq;
import com.device.manager.main.vo.reqeust_entity.QueryFarmReq;

import java.util.Map;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/8 17:36
 * @file: FarmService
 * @description: 星期一
 */
public interface FarmService {

    /**
     * 管理员和普通用户公用的接口
     **/

    TResponse<Integer> deleteFarmById(int id);

    TResponse<Integer> deleteFarmByIds(int[] ids);

    TResponse<Integer> updateFarm(Farm farm);

    int deleteFarmByUserName(String userName);

    int deleteFarmByUserNames(String[] userNames);

    int itemSize(Map<String, Object> map);

    TResponse<BaseResp> getFarmByPageHelper(QueryFarmReq req);

    /**
     * 普通用户使用的接口
     **/
    TResponse<Integer> registFarm(RegistFarmReq req);

    /**
     * 管理员使用的接口
     **/
    int insertFarm(Farm farm);
}
