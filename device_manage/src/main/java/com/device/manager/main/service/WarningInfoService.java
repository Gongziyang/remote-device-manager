package com.device.manager.main.service;

import com.device.manager.main.vo.entity.DeviceWarning;
import com.device.manager.main.vo.msg_standard.TResponse;
import com.device.manager.main.vo.reqeust_entity.BaseResp;
import com.device.manager.main.vo.reqeust_entity.QueryWarningInfoReq;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/10 15:04
 * @file: WarningInfoService
 * @description: 星期三
 */
public interface WarningInfoService {

    int itemSize(QueryWarningInfoReq warningInfoReq);

    TResponse<BaseResp> queryWarningsInfoByPageHelper(QueryWarningInfoReq warningInfoReq);

    TResponse<Integer> deleteById(int id);

    TResponse<Integer> deleteByIds(int[] ids);

    int insertWarning(DeviceWarning deviceWarning);

}
