package com.device.manager.main.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.device.manager.main.mapper.WaringInfoMapper;
import com.device.manager.main.service.WarningInfoService;
import com.device.manager.main.utils.custom.ResponseUtils;
import com.device.manager.main.vo.entity.DeviceWarning;
import com.device.manager.main.vo.msg_standard.TResponse;
import com.device.manager.main.vo.reqeust_entity.BaseResp;
import com.device.manager.main.vo.reqeust_entity.QueryWarningInfoReq;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/10 15:06
 * @file: WarningInfoServiceImpl
 * @description: 星期三
 */
@Service
public class WarningInfoServiceImpl implements WarningInfoService {

    @Autowired
    WaringInfoMapper waringInfoMapper;

    @Override
    public int itemSize(QueryWarningInfoReq warningInfoReq) {
        return waringInfoMapper.itemSize(warningInfoReq);
    }

    @Override
    public TResponse<BaseResp> queryWarningsInfoByPageHelper(QueryWarningInfoReq warningInfoReq) {
        System.out.println("[query-warning-info]"+ JSONObject.toJSONString(warningInfoReq));
        int pageNo = warningInfoReq.getPageNo();
        int pageSize = warningInfoReq.getPageSize();
        int itemSize = itemSize(warningInfoReq);
        TResponse<BaseResp> response = new TResponse<>();
        PageHelper.startPage(pageNo, pageSize);
        //可模糊查询
        List<DeviceWarning> deviceWarnings = waringInfoMapper.queryWarningsByPageHelper(warningInfoReq);
        BaseResp data = new BaseResp();
        int pageCount = itemSize % pageSize == 0 ? itemSize / pageSize : (itemSize / pageSize + 1);
        if (deviceWarnings == null) {
            ResponseUtils.set(response, -1, "查询失败", null);
        } else {
            data.setPageNo(pageNo);
            data.setItemSize(itemSize);
            data.setPageCount(pageCount);
            data.setList(deviceWarnings);
            ResponseUtils.set(response, 0, "查询成功", data);
        }
        String timeStamp = getTimeStamp();
        response.setTimeStamp(timeStamp);
        return response;
    }

    @Override
    public TResponse<Integer> deleteById(int id) {
        TResponse<Integer> response = new TResponse<>();
        int affect = waringInfoMapper.deleteById(id);
        if (affect == 0) {

            ResponseUtils.set(response, -1, "删除出错", null);
        } else {

            ResponseUtils.set(response, 0, "操做成功", affect);
        }
        String timeStamp = getTimeStamp();
        response.setTimeStamp(timeStamp);
        return response;
    }

    @Override
    public TResponse<Integer> deleteByIds(int[] ids) {
        Map<String, Object> idsMap = new HashMap<>(16);
        idsMap.put("ids", ids);
        TResponse<Integer> response = new TResponse<>();
        int affect = waringInfoMapper.deleteByIds(idsMap);
        if (affect == 0) {

            ResponseUtils.set(response, -1, "删除出错", null);
        } else {

            ResponseUtils.set(response, 0, "操作成功", affect);
        }
        String timeStamp = getTimeStamp();
        response.setTimeStamp(timeStamp);
        return response;
    }

    @Override
    public int insertWarning(DeviceWarning deviceWarning) {
        return waringInfoMapper.insertWarning(deviceWarning);
    }

    private String getTimeStamp() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String timeStamp = dateFormat.format(System.currentTimeMillis());
        return timeStamp;
    }
}
