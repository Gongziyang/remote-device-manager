package com.device.manager.main.utils.custom;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/17 0:31
 * @file: BeanUtils
 * @description: 星期三
 */
@Component
public class BeanUtils implements ApplicationContextAware {

    protected static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext arg) throws BeansException {
        if (applicationContext == null) {
            applicationContext = arg;
        }
    }

    /**
     * name : 通过名字获取指定的bean,一般配合@Bean(name="xxx")使用，若不存在新创建一个
     **/
    public static Object getBean(String name) {
        return applicationContext.getBean(name);
    }

    /**
     * 通过class获取bean，如不存在新创建一个
     **/
    public static <T> T getBean(Class<T> z) {
        return applicationContext.getBean(z);
    }

    /**
     * 通过class返回指定名称的bean,若不存在则新创建一个
     **/
    public static <T> T getBean(String name, Class<T> z) {
        return applicationContext.getBean(name, z);
    }

    /**
     * 获取请求中的origin部分
     **/
    public static String getOrigin() {
        HttpServletRequest request = getHttpServletRequest();
        return request.getHeader("Origin");
    }

    public static HttpServletRequest getHttpServletRequest() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    }

    public static String getDomain() {
        HttpServletRequest request = getHttpServletRequest();
        StringBuffer url = request.getRequestURL();
        return url.delete(url.length() - request.getRequestURL().length(), url.length()).toString();
    }
}
