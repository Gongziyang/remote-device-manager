package com.device.manager.main.vo.sys_user;

import org.apache.shiro.authc.AuthenticationToken;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/19 9:35
 * @file: JwtToken
 * @description: 星期五
 */
public class JwtToken implements AuthenticationToken {

    private static final long serialVersionUID = 1L;
    private String token;

    public JwtToken(String token) {
        this.token = token;
    }

    @Override
    public Object getPrincipal() {
        return token;
    }

    @Override
    public Object getCredentials() {
        return token;
    }
}
