package com.device.manager.main.vo.reqeust_entity;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/17 17:23
 * @file: CtrlResp
 * @description: 星期三
 */
public class CtrlResp {

    private int result;

    private String info;

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
