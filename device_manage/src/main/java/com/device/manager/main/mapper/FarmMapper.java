package com.device.manager.main.mapper;

import com.device.manager.main.vo.entity.Device;
import com.device.manager.main.vo.entity.Farm;
import com.device.manager.main.vo.reqeust_entity.QueryFarmReq;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/8 17:29
 * @file: FarmMapper
 * @description: 星期一
 */
@Mapper
public interface FarmMapper {


    /**
     * 管理员和普通用户公用接口
     **/
    public int deleteFarmById(int id);

    public int deleteFarmByIds(Map<String, Object> farmIds);

    public int deleteFarmByUserNames(Map<String, Object> userNames);

    public int deleteFarmByUserName(String userName);

    public int updateFarm(Farm farm);

    public int itemSize(Map<String, Object> map);

    public List<Farm> getFarmByPageHelper(QueryFarmReq req);


    /**
     * 普通用户使用的接口
     **/
    public int registFarm(Farm farm);

    /**
     * 管理员使用的接口
     **/
    public int insertFarm(Farm farm);

    List<Farm> getFarmStat(String userName);
}
