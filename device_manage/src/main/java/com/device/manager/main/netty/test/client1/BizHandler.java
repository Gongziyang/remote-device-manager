package com.device.manager.main.netty.test.client1;

import com.alibaba.fastjson.JSONObject;
import com.device.manager.main.netty.msg.*;
import io.netty.channel.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/18 10:48
 * @file: BizHandler
 * @description: 星期四
 */
public class BizHandler {


    private Logger logger = LoggerFactory.getLogger(BizHandler.class);

    /**
     * @param baseInf :收到的回复报文
     *                description:收到服务端的回复报文
     **/
    public void relpyHandle(BaseInf<Object> baseInf) {
        logger.info("Client-received a relpy info", JSONObject.toJSONString(baseInf));
        int seq = baseInf.getSeq();
        String seqStr = String.valueOf(seq);
        int seqType = Integer.parseInt(seqStr.substring(0, 1));
        switch (seqType) {
            case 1:
                logger.info("Client-BizHandle-case-1:{}", "[土壤湿度检测器]这是一个登录回复报文，服务端已收到登录信息");
                break;
            case 2:
                logger.info("Client-BizHandle-case-2:{}", "[土壤湿度检测器]这是一个警报回复报文，服务端已收到警报信息");
                break;
        }
    }

    /**
     * @param baseInf :收到的控制报文
     **/
    public void ctrlHandle(BaseInf<Object> baseInf, Channel channel) {
        logger.info("Client-BizHandle:{}", "[土壤湿度检测器]收到一个控制设备报文，正在处理");
        CtrlMsg ctrlMsg = (CtrlMsg) baseInf.getMsg();
        int cmd = ctrlMsg.getCmd();
        switch (cmd) {
            case 0:
                logger.info("Client-BizHandle-case-0:{}", "设备[土壤湿度检测器]收到一个启动命令，启动中...");
                reply(baseInf, channel);
                /** underlying code **/
                break;
            case 1:
                logger.info("Client-BizHandle-case-0:{}", "设备[土壤湿度检测器]收到一个重启命令，重新启动中...");
                reply(baseInf, channel);
                /** underlying code **/
                break;
            case 2:
                logger.info("Client-BizHandle-case-0:{}", "设备[土壤湿度检测器]收到一个暂停命令，暂停中...");
                reply(baseInf, channel);
                /** underlying code **/
                break;
            case 3:
                logger.info("Client-BizHandle-case-0:{}", "设备[土壤湿度检测器]收到一个关闭命令，关闭中...");
                reply(baseInf, channel);
                /** underlying code **/
                break;
            default:
                break;
        }
    }

    /**================================================================**/

    /**
     * @param channel : 对应的信道
     *                description : 发送登录报文
     **/
    public void sendLogin(Channel channel) {
        BaseInf<DeviceLoginMsg> baseInf = new BaseInf<>();
        baseInf.setVersion((short) 10);
        baseInf.setCmd((byte) 1);
        DeviceLoginMsg loginMsg = new DeviceLoginMsg();
        loginMsg.setDeviceId(25);
        loginMsg.setLoginTime("2020-10-03");
        baseInf.setMsg(loginMsg);
        baseInf.setSeq(10000);
        baseInf.setType((short) 1);
        channel.writeAndFlush(baseInf);
    }

    /**
     * @param channel : 对应的信道
     *                description : 发送报警报文
     **/
    public void sendWarning(Channel channel) {
        BaseInf<WarningMsg> baseInf = new BaseInf<>();
        baseInf.setVersion((short) 10);
        baseInf.setCmd((byte) 2);
        WarningMsg warningMsg = new WarningMsg();
        warningMsg.setFarmId(1);
        warningMsg.setWarningInfo("位于雅安雨城区的大棚土壤湿度较低，请及时处理");
        warningMsg.setWarningName("水份检测器设备报警");
        warningMsg.setDeviceId(16);
        baseInf.setMsg(warningMsg);
        baseInf.setSeq(20000);
        baseInf.setType((short) 1);
        baseInf.setDeviceId(25);
        channel.writeAndFlush(baseInf);
    }

    public void reply(BaseInf<Object> baseInf, Channel channel) {
        System.out.println("---------------------------------------\n");
        RetMsg retMsg = new RetMsg();
        retMsg.setResult(0);
        retMsg.setInfo("操作成功");

        //回复
        baseInf.setType((short) 1);
        baseInf.setCmd((byte) 4);
        baseInf.setMsg(retMsg);
        channel.writeAndFlush(baseInf);
        System.out.println("[温度检测器]回复服务端的控制请求...");
        logger.info("Client-BizHandle-received a pkt:{}", "[温度检测器] send a reply pkt");
        System.out.println("+++++++++++++++++++++++++++++++++++++++\n");
    }
}
