package com.device.manager.main.mapper.sys_user;

import com.device.manager.main.vo.sys_user.SysMenu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/19 10:34
 * @file: SysPermissionMapper
 * @description: 星期五
 */
@Mapper
public interface SysMenuMapper {

    /**
     * 根据用户查询用户权限
     */
    public List<SysMenu> queryByUser(@Param("username") String username);

}
