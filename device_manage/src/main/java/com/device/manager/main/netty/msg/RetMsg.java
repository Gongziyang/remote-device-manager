package com.device.manager.main.netty.msg;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/16 14:29
 * @file: RetMsg
 * @description: 星期二
 */
public class RetMsg {

    /**
     * 处理结果   0 成功  1失败
     **/
    int result;
    /**
     * 说明信息
     **/
    String info;

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
