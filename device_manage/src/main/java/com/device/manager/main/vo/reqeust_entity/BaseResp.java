package com.device.manager.main.vo.reqeust_entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/21 15:05
 * @file: BaseResp
 * @description: 星期日
 */
@ApiModel(description = "分页查询后台响应的数据放在data字段中，包含pageNo,pageSize,list")
public class BaseResp {

    @ApiModelProperty(name = "pageNo", value = "当前页码", dataType = "Integer", example = "1")
    private int pageNo;
    @ApiModelProperty(name = "itemSize", value = "数据总量", dataType = "Integer", example = "10")
    private int itemSize;
    @ApiModelProperty(name = "pageCount", value = "总共页数", dataType = "Integer", example = "3")
    private int pageCount;
    @ApiModelProperty(name = "list", value = "数据集合", dataType = "List", example = "List[]")
    private Object list;

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getItemSize() {
        return itemSize;
    }

    public void setItemSize(int itemSize) {
        this.itemSize = itemSize;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public Object getList() {
        return list;
    }

    public void setList(Object list) {
        this.list = list;
    }
}
