package com.device.manager.main.vo.sys_user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/19 9:41
 * @file: SysPermission
 * @description: 星期五
 */
public class SysMenu implements Serializable {


    private static final long serialVersionUID = 1L;

    /**
     * id 唯一标识
     */
    private int id;

    /**
     * 父id,用于构建层级菜单
     */
    private int parentId;

    /**
     * 对应vue路由中的name
     */
    private String name;

    /**
     * 地址路径
     */
    private String path;

    /***
     * 中文名称，用于渲染vue中的导航菜单名
     * */
    private String nameZh;

    /**
     * 组件名，用于解析路由对应的组件
     **/
    private String component;

    /**
     * element-ui图标类，用于渲染菜单栏
     */
    private String iconCls;

    /**
     * 子菜单
     **/
    private List<SysMenu> children = new ArrayList<>(1000);

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getNameZh() {
        return nameZh;
    }

    public void setNameZh(String nameZh) {
        this.nameZh = nameZh;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getIconCls() {
        return iconCls;
    }

    public void setIconCls(String iconCls) {
        this.iconCls = iconCls;
    }

    public List<SysMenu> getChildren() {
        return children;
    }

    public void setChildren(List<SysMenu> children) {
        this.children = children;
    }
}
