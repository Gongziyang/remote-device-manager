package com.device.manager.main.utils.custom;

import java.nio.charset.StandardCharsets;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/29 9:56
 * @file: CRCUtil
 * @description: 星期一
 */
public class CrcUtil {


    /**
     * 使用CRC_16_CCITT_FALSE算法
     * init : crc寄存器的值，初始值为0xffff
     * polynomial: 多项式，参与生成CRC校验码,默认值为0x1021;
     * 原理：
     * 1、先将crc寄存器的值与字节流的第一个字节进行异或，将值存到寄存器中，高地址的8位不变
     * 2、将crc寄存器朝低地址方向左移一位，判断溢出的数为0还是为1，如果是0，则继续左移，如果是1，将crc寄存器与polynomial进行异或
     * 3、重复第二步，知道将寄存器中低地址的八位全部溢出为止，移动8次
     * 4、重复1 2 3 步骤，最后得到crc校验码
     * 5、取后16位为crc校验码
     **/
    public static short crc_16_ccitt_false(byte[] bytes) {
        short init = (short) 0xffff;
        short polynomial = (short) 0x1021;
        for (int i = 0; i < bytes.length; i++) {
            //依次取出数据源中的字节
            byte b = bytes[i];
            for (int j = 0; j < 8; j++) {
                //一个字节有八位，从头第一位bit开始，判断它是1还是0。
                boolean bit = ((b >> (7 - j) & 1) == 1);
                //将init右移15位，然后与1相与，判断与b对应的bit是0还是1
                boolean crcBit = ((init >> 15 & 1) == 1);
                //左移1位，保证每次检查的bit和b中对齐
                init = (short) (init << 1);
                //判断两个bit异或的结果,也就是上面说的寄存器每次溢出的那个bit
                if (bit ^ crcBit) {
                    //异或：相同为0，不同为1，这里表明上面两个bit异或的结果是1
                    init = (short) (init ^ polynomial);
                }
            }
        }
        //最后只取出最后16位
//        init = init & 0xffff;
        //转为16进制并返回
        return init;
    }


    /**
     * @param data 待校验的数据
     * @param crc  循环冗余码
     **/
    public static boolean checkCrc(byte[] data, short crc) {

        short crcRegister = (short) 0xffff;
        short polynomial = (short) 0x1021;
        for (int i = 0; i < data.length; i++) {
            byte b = data[i];
            for (int j = 0; j < 8; j++) {
                boolean bit = ((b >> (7 - j) & 1) == 1);
                boolean crcBit = ((crcRegister >> 15 & 1) == 1);
                crcRegister = (short) (crcRegister << 1);
                if (bit ^ crcBit) {
                    crcRegister = (short) (crcRegister ^ polynomial);
                }
            }
        }
        //取后16位，2个字节的长度
//        crcRegister = crcRegister & 0xffff;
        if (crcRegister == crc) {
            return true;
        }

        System.out.println("[crc]" + crc);
        System.out.println("[genCrc]" + crcRegister);
        //判断是否和校验码相同
        return false;
    }


    /**
     * test
     **/
    public static void main(String[] args) {
        byte[] data = {(byte) 0xAA, 0x0C, 0x01, 0x00, 0x01, 0x00, 0x00, 0x04, 0x05, 0x17, 0x05, 0x01, (byte) 0xA0, (byte) 0x86, 0x01, 0x00};
        short crc = crc_16_ccitt_false(data);
        System.out.println("[(short)crc]" + String.valueOf(crc));
        System.out.println("[(Hex)crc]" + Integer.toHexString(crc).toUpperCase().substring(4));
        boolean checkResult = checkCrc(data, crc);
        System.out.println("[checkResult]" + checkResult);
    }
}
