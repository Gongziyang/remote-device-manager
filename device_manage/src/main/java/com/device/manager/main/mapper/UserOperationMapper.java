package com.device.manager.main.mapper;

import com.device.manager.main.vo.entity.UserOperation;
import com.device.manager.main.vo.reqeust_entity.InsertOperation;
import com.device.manager.main.vo.reqeust_entity.QueryOperationReq;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/8 23:31
 * @file: UserOperationMapper
 * @description: 星期一
 */
@Mapper
public interface UserOperationMapper {

    //用户注册设备或者农场的时候需要用到该方法
    public int insertOperation(InsertOperation operation);

    public List<UserOperation> queryOperationByPageHelper(QueryOperationReq operationReq);

    public int itemSize(String userName);

    public int deleteById(int id);

    public int deleteByIds(Map<String, Object> idsMap);

    public int updateStatus(Map<String, Object> map);

}
