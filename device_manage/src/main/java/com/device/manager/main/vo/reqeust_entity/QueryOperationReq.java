package com.device.manager.main.vo.reqeust_entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/10 16:06
 * @file: QueryOperationReq
 * @description: 星期三
 */
@ApiModel(description = "包含查询操作记录所需要的参数")
public class QueryOperationReq extends BaseReq {

    @ApiModelProperty(name = "userName", value = "用户名", dataType = "String", example = "yyf")
    private String userName = null;
    //模糊查询
    @ApiModelProperty(name = "fuzzyName", value = "模糊查询字段", dataType = "String", example = "abcd")
    private String fuzzyName = null;
    @ApiModelProperty(name = "date", value = "日期", dataType = "String", example = "2020-09-01")
    private String date = null;

    @Override
    public int getPageNo() {
        return pageNo;
    }

    @Override
    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    @Override
    public int getPageSize() {
        return pageSize;
    }

    @Override
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFuzzyName() {
        return fuzzyName;
    }

    public void setFuzzyName(String fuzzyName) {
        this.fuzzyName = fuzzyName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
