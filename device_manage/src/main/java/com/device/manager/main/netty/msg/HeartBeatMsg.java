package com.device.manager.main.netty.msg;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/17 10:56
 * @file: HeartBeatMsg
 * @description: 星期三
 */
public class HeartBeatMsg {

    private String heartBeatTime;


    public String getHeartBeatTime() {
        return heartBeatTime;
    }

    public void setHeartBeatTime(String heartBeatTime) {
        this.heartBeatTime = heartBeatTime;
    }
}
