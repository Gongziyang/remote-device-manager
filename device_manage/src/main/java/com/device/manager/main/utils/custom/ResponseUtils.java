package com.device.manager.main.utils.custom;

import com.device.manager.main.vo.msg_standard.TResponse;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/21 15:41
 * @file: ReponseUtils
 * @description: 星期日
 */
public class ResponseUtils {

    public static void set(TResponse response, int ret, String msg, Object data) {
        response.setData(data);
        response.setMsg(msg);
        response.setRet(ret);
    }
}
