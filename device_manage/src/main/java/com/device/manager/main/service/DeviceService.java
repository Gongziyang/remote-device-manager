package com.device.manager.main.service;

import com.device.manager.main.vo.entity.Device;
import com.device.manager.main.vo.msg_standard.TResponse;
import com.device.manager.main.vo.reqeust_entity.BaseResp;
import com.device.manager.main.vo.reqeust_entity.QueryDeviceReq;
import com.device.manager.main.vo.reqeust_entity.RegistDeviceReq;

import java.util.List;
import java.util.Map;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/9 14:01
 * @file: DeviceMapper
 * @description: 星期二
 */
public interface DeviceService {

    /**
     * 公用接口
     * deleteDevicesByFarmId和deleteDevicesByFarmIds两个接口主要是用于删除农场时要删除关联设备
     **/
    int itemSize(Map<String, Object> map);

    int deleteDevicesByFarmId(int farmId);

    int deleteDevicesByFarmIds(int[] farmIds);

    TResponse<Integer> deleteDeviceById(int id);

    TResponse<Integer> deleteDeviceByIds(int[] ids);

    public int deleteDevicesByUserName(String userName);

    public int deleteDevicesByUserNames(String[] userNames);

    TResponse<BaseResp> getDevicesByPageHelper(QueryDeviceReq req);

    TResponse<Integer> updateDevice(Device device);

    /**
     * 一般用户注册设备所用到的接口
     **/
    TResponse<Integer> registDevice(RegistDeviceReq deviceReq);

    /**
     * 管理员所用接口
     **/
    int insertDevice(Device device);


    int updateState(int deviceId, int status);

    int updateLoginTime(int deviceId, String loginTime);

    List<Integer> getDeviceIdsByUserName(String userName);

    List<Integer> getDeviceIds();
}
