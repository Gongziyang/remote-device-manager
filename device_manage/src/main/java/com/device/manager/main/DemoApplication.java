package com.device.manager.main;

import com.device.manager.main.netty.client.Client;
import com.device.manager.main.netty.server.Server;
import io.swagger.annotations.ApiModel;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.apache.shiro.spring.boot.autoconfigure.ShiroAnnotationProcessorAutoConfiguration;
import org.apache.shiro.spring.boot.autoconfigure.ShiroAutoConfiguration;
import org.apache.shiro.spring.boot.autoconfigure.ShiroBeanAutoConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Log4j2
@SpringBootApplication(exclude = {ShiroAutoConfiguration.class,
        ShiroAnnotationProcessorAutoConfiguration.class,
        ShiroBeanAutoConfiguration.class})
public class DemoApplication {

    public static void main(String[] args) throws InterruptedException {

        SpringApplication.run(DemoApplication.class, args);

        new Thread(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                Thread.sleep(5000);
                Client client = new Client();
                client.connect();
            }
        }).start();
        //启动netty服务端
        Server server = new Server();
        server.start();


    }
}
