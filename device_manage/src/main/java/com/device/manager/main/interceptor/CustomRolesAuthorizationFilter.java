package com.device.manager.main.interceptor;

import com.alibaba.fastjson.JSONObject;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authz.AuthorizationFilter;
import org.springframework.stereotype.Component;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/22 9:16
 * @file: CustomRolesAuthorizationFilter
 * @description: 星期一
 */
@Component
public class CustomRolesAuthorizationFilter extends AuthorizationFilter {
    @Override
    protected boolean isAccessAllowed(ServletRequest servletRequest, ServletResponse servletResponse, Object o) throws Exception {
        Subject subject = getSubject(servletRequest, servletResponse);
        //拿到用户的角色集合
        String[] roles = (String[]) o;
        System.out.println("[角色集合]" + JSONObject.toJSONString(roles));
        //遍历角色集合，只要当前登录用拥有其中一个角色就放行
        for (int i = 0; i < roles.length; i++) {
            if (subject.hasRole(roles[i])) {
                return true;
            }
        }
        return false;
    }
}
