package com.device.manager.main.netty.msg;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/16 14:29
 * @file: CtrlMsg
 * @description: 星期二
 */
public class CtrlMsg {

    public int cmd;

    public int getCmd() {
        return cmd;
    }

    public void setCmd(int cmd) {
        this.cmd = cmd;
    }
}
