package com.device.manager.main.netty;

import com.alibaba.fastjson.JSONObject;
import com.device.manager.main.netty.msg.*;
import com.device.manager.main.utils.custom.CrcUtil;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/16 20:00
 * @file: Encodeer
 * @description: 星期二
 */
public class Encoder extends MessageToByteEncoder<BaseInf<Object>> {

    public static Logger logger = LoggerFactory.getLogger(Decoder.class);

    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext,
                          BaseInf<Object> baseInf,
                          ByteBuf byteBuf) throws Exception {
        System.out.println("+++++++++++++++++++++++++++++++++++++++");
        logger.info("Encoder:{}", "准备编码...");
        int msgLen = 0;
        short totalLen = 0;
        byte[] bytes;
        short version = baseInf.getVersion();
        byte cmd = baseInf.getCmd();
        int seq = baseInf.getSeq();
        short type = baseInf.getType();
        int deviceId = baseInf.getDeviceId();

        if (type == Constant.PLATFORM_TO_DEVICE) {
            switch (cmd) {
                case Constant.CONTROL_MSG:
                    //发送控制报文，平台发送控制指令的时候用
                    logger.info("Encoder:{}", "该报文是平台控制报文，编码中...");
                    CtrlMsg ctrlMsg = (CtrlMsg) baseInf.getMsg();
                    bytes = JSONObject.toJSONString(ctrlMsg).getBytes();
                    msgLen = bytes.length;
                    totalLen = (short) (msgLen + Constant.HEADER_SIZE);
                    write(byteBuf, version, totalLen, seq, type, cmd, deviceId, bytes);
                    break;
                case Constant.REPLY_MSG:
                    logger.info("Encoder:{}", "该报文是响应报文，编码中...");
                    //发送回复报文，平台和设备都要用
                    RetMsg retMsg = (RetMsg) baseInf.getMsg();
                    bytes = JSONObject.toJSONString(retMsg).getBytes();
                    msgLen = bytes.length;
                    totalLen = (short) (msgLen + Constant.HEADER_SIZE);
                    write(byteBuf, version, totalLen, seq, type, cmd, deviceId, bytes);
                    break;
                default:
                    break;
            }
        }
        if (type == Constant.DEVICE_TO_PLATFORM) {
            switch (cmd) {
                case Constant.LOGIN_MSG:
                    logger.info("Encoder:{}", "该报文是设备登录报文，编码中...");
                    //发送登录报文，设备登录的时候调用
                    DeviceLoginMsg loginMsg = (DeviceLoginMsg) baseInf.getMsg();
                    bytes = JSONObject.toJSONString(loginMsg).getBytes();
                    msgLen = bytes.length;
                    totalLen = (short) (msgLen + Constant.HEADER_SIZE);
                    write(byteBuf, version, totalLen, seq, type, cmd, deviceId, bytes);
                    break;
                case Constant.WARN_MSG:
                    logger.info("Encoder:{}", "该报文是设备报警报文，编码中...");
                    //发送警报报文，设备报警的时候用
                    WarningMsg warningMsg = (WarningMsg) baseInf.getMsg();
                    String jsonString = JSONObject.toJSONString(warningMsg);
                    bytes = jsonString.getBytes();
                    msgLen = bytes.length;
                    totalLen = (short) (msgLen + Constant.HEADER_SIZE);
                    write(byteBuf, version, totalLen, seq, type, cmd, deviceId, bytes);
                    break;
                case 5:
                    break;
                case Constant.REPLY_MSG:
                    logger.info("Encoder:{}", "该报文是响应报文，编码中...");
                    //发送回复报文，平台和设备都要用
                    RetMsg retMsg = (RetMsg) baseInf.getMsg();
                    bytes = JSONObject.toJSONString(retMsg).getBytes();
                    msgLen = bytes.length;
                    totalLen = (short) (msgLen + Constant.HEADER_SIZE);
                    write(byteBuf, version, totalLen, seq, type, cmd, deviceId, bytes);
                    break;
                default:
                    break;
            }
        }
    }

    public void write(ByteBuf byteBuf, short version, short len, int seq,
                      short type, byte cmd, int deviceId, byte[] bytes) {
        byteBuf.writeShort(version);
        //再将报文长度写入 2个字节
        byteBuf.writeShort(len);
        //写入报文序号
        byteBuf.writeInt(seq);
        //写入类型
        byteBuf.writeShort(type);
        //再将cmd写入  1个字节
        byteBuf.writeByte(cmd);
        //再将deviceId写入 4个字节
        byteBuf.writeInt(deviceId);
        //再将msg的字节数组写入
        byteBuf.writeBytes(bytes);
        //计算crc校验码 2 个字节
        short crc = CrcUtil.crc_16_ccitt_false(bytes);
        byteBuf.writeShort(crc);
        logger.info("Encoder:{}", "成功完成报文的编码，提交到下一个handler处理");
    }
}
