package com.device.manager.main.vo.reqeust_entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/9 15:50
 * @file: QueryDeviceReq
 * @description: 星期二
 */
@ApiModel(description = "包含查询设备信息的请求参数")
public class QueryDeviceReq extends BaseReq {

    /**
     * 当普通用户查询时需要将userId带上，当查某个农场对应的设备列表时，需要将farmId带上
     * 当通过设备名称和设备编号来查询时，需要将deviceName和deviceNumber带上
     **/
    @ApiModelProperty(name = "farmId", value = "农场id", dataType = "Integer", example = "001")
    private int farmId = -1;
    @ApiModelProperty(name = "userName", value = "用户名", dataType = "String", example = "yyf")
    private String userName = null;
    @ApiModelProperty(name = "deviceName", value = "设备名称", dataType = "String", example = "温度监控器")
    private String deviceName = null;
    @ApiModelProperty(name = "deviceNumber", value = "设备编号", dataType = "String", example = "WD023")
    private String deviceNumber = null;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public int getPageNo() {
        return pageNo;
    }

    @Override
    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    @Override
    public int getPageSize() {
        return pageSize;
    }

    @Override
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public int getFarmId() {
        return farmId;
    }

    public void setFarmId(int farmId) {
        this.farmId = farmId;
    }
}
