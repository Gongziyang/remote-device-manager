package com.device.manager.main.netty.server;

import com.device.manager.main.netty.Decoder;
import com.device.manager.main.netty.Encoder;
import com.device.manager.main.netty.PackageSpilter;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.util.concurrent.FutureListener;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/15 23:14
 * @file: NettyServer
 * @description: 星期一
 */
public class Server {


    private static final int SO_BACKLOG = 128;
    private static final int PORT = 8888;

    public void start() {
        ServerBootstrap serverBootstrap = new ServerBootstrap();
        EventLoopGroup boss = new NioEventLoopGroup();
        EventLoopGroup worker = new NioEventLoopGroup();
        serverBootstrap.group(boss, worker);
        serverBootstrap.childHandler(new ChannelInitializer<SocketChannel>() {
            @Override
            protected void initChannel(SocketChannel channel) throws Exception {
                channel.pipeline().addLast(new PackageSpilter());
                channel.pipeline().addLast(new Decoder());
                channel.pipeline().addLast(new Encoder());
                channel.pipeline().addLast(new ServerHandler());

            }
        });
        serverBootstrap.channel(NioServerSocketChannel.class);
        serverBootstrap.option(ChannelOption.SO_BACKLOG, SO_BACKLOG);
        serverBootstrap.childOption(ChannelOption.SO_KEEPALIVE, true);
        try {
            //同步绑定，确保绑定完成，如果不同步，可用future.addListener(ChannelFutureListener)来实现回调
            ChannelFuture future = serverBootstrap.bind(PORT).sync();
            System.out.println("Netty-Server is running on port "+PORT);
            //等待客户端链路关闭，同步关闭
            future.channel().closeFuture().sync();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            boss.shutdownGracefully();
            worker.shutdownGracefully();
        }
    }
}
