package com.device.manager.main.vo.msg_standard;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/7 23:07
 * @file: TResponse
 * @description: 星期日
 */
public class TResponse<T> implements Serializable {


    private String TimeStamp;  //时间戳

    private int Ret;  //返回状态

    private String Msg;   //说明

    private T Data;  //返回数据


    public String getTimeStamp() {
        return TimeStamp;
    }


    public void setTimeStamp(String timeStamp) {
        TimeStamp = timeStamp;
    }


    public int getRet() {
        return Ret;
    }

    public void setRet(int ret) {
        Ret = ret;
    }

    public String getMsg() {
        return Msg;
    }

    public void setMsg(String msg) {
        Msg = msg;
    }

    public T getData() {
        return Data;
    }

    public void setData(T data) {
        Data = data;
    }
}
