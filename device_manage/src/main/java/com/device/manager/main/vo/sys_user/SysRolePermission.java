package com.device.manager.main.vo.sys_user;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/21 12:31
 * @file: SysRolePermission
 * @description: 星期日
 */
public class SysRolePermission {


    private int id;

    private int rid;

    private int pid;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRid() {
        return rid;
    }

    public void setRid(int rid) {
        this.rid = rid;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }
}
