package com.device.manager.main.service.impl;

import com.device.manager.main.mapper.RegistFarmRecordMapper;
import com.device.manager.main.service.RegistFarmRecordService;
import com.device.manager.main.vo.entity.Farm;
import com.device.manager.main.vo.reqeust_entity.InsertRegistFarmRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/8 23:57
 * @file: RegistFarmRecordServiceImpl
 * @description: 星期一
 */
@Service
public class RegistFarmRecordServiceImpl implements RegistFarmRecordService {

    @Autowired
    RegistFarmRecordMapper registFarmRecordMapper;

    @Override
    public int insertRecord(InsertRegistFarmRecord registFarmRecord) {
        return registFarmRecordMapper.insertRecord(registFarmRecord);
    }

    @Override
    public Farm getFarmByOptionIdAndUserName(String userName, int operationId) {
        Map<String, Object> map = new HashMap<>(16);
        map.put("userName", userName);
        map.put("operationId", operationId);
        return registFarmRecordMapper.getFarmByOptionIdAndUserName(map);
    }

    @Override
    public int deleteByOptionIdAndUserName(String userName, int operationId) {
        Map<String, Object> map = new HashMap<>(16);
        map.put("userName", userName);
        map.put("operationId", operationId);
        return registFarmRecordMapper.deleteByOptionIdAndUserName(map);
    }
}
