package com.device.manager.main.utils.common;

import com.device.manager.main.constant.Constant;
import com.device.manager.main.vo.msg_standard.TRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author: yyf
 * @create: 2021 04 2021/4/15 17:49
 * @file: PramCheckUtil
 * @description: 星期四
 */
public class ParamCheckUtil {

    static Logger logger = LoggerFactory.getLogger(ParamCheckUtil.class);

    public static void handlerError(Object obj) {
        TRequest<Object> request = (TRequest<Object>) obj;
        if (request == null && request.getData() == null) {
            throw new RuntimeException("Params are unCorrect");
        }

    }
}
