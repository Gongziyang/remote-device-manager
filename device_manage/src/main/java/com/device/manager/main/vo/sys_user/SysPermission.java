package com.device.manager.main.vo.sys_user;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/21 12:30
 * @file: SysPermission
 * @description: 星期日
 */
public class SysPermission {

    /**
     * id 唯一标识
     **/
    private int id;

    /**
     * name 权限名称
     */
    private String name;

    /**
     * description : 描述
     **/
    private String desc;


    /**
     * url : 访问路径
     **/
    private String url;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
