package com.device.manager.main.mapper;

import com.device.manager.main.vo.reqeust_entity.QueryUserReq;
import com.device.manager.main.vo.sys_user.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/11 15:03
 * @file: UserMapper
 * @description: 星期四
 */
@Mapper
public interface UserMapper {

    public int itemSize();

    public List<SysUser> getUserByPageHelper(QueryUserReq req);

    public int deleteUserById(String userName);

    public int deleteUserByIds(Map<String, Object> userIds);

    public int updateUser(SysUser user);

    /**
     * 通过用户账号查询用户信息
     *
     * @param username
     * @return
     */
    SysUser getUserByName(@Param("username") String username);

    int regist(SysUser user);
}
