package com.device.manager.main.vo.reqeust_entity;

/**
 * @author: yyf
 * @create: 2021 04 2021/4/16 22:03
 * @file: StatInfo
 * @description: 星期五
 */
public class StatInfoResp {
    int fruitFarmCount = 0;
    int yzFarmCount = 0;
    int nzwFarmCount = 0;
    int onlineFarm = 0;
    int totalFarm = 0;
    int closedFarm = 0;
    int onlineDevice = 0;
    int tempDevice = 0;
    int closedDevice = 0;
    int totalDevice = 0;

    public int getFruitFarmCount() {
        return fruitFarmCount;
    }

    public void setFruitFarmCount(int fruitFarmCount) {
        this.fruitFarmCount = fruitFarmCount;
    }

    public int getYzFarmCount() {
        return yzFarmCount;
    }

    public void setYzFarmCount(int yzFarmCount) {
        this.yzFarmCount = yzFarmCount;
    }

    public int getNzwFarmCount() {
        return nzwFarmCount;
    }

    public void setNzwFarmCount(int nzwFarmCount) {
        this.nzwFarmCount = nzwFarmCount;
    }

    public int getOnlineFarm() {
        return onlineFarm;
    }

    public void setOnlineFarm(int onlineFarm) {
        this.onlineFarm = onlineFarm;
    }

    public int getTotalFarm() {
        return totalFarm;
    }

    public void setTotalFarm(int totalFarm) {
        this.totalFarm = totalFarm;
    }

    public int getClosedFarm() {
        return closedFarm;
    }

    public void setClosedFarm(int closedFarm) {
        this.closedFarm = closedFarm;
    }

    public int getOnlineDevice() {
        return onlineDevice;
    }

    public void setOnlineDevice(int onlineDevice) {
        this.onlineDevice = onlineDevice;
    }

    public int getTempDevice() {
        return tempDevice;
    }

    public void setTempDevice(int tempDevice) {
        this.tempDevice = tempDevice;
    }

    public int getClosedDevice() {
        return closedDevice;
    }

    public void setClosedDevice(int closedDevice) {
        this.closedDevice = closedDevice;
    }

    public int getTotalDevice() {
        return totalDevice;
    }

    public void setTotalDevice(int totalDevice) {
        this.totalDevice = totalDevice;
    }
}
