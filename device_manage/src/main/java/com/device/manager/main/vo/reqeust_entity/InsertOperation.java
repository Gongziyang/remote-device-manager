package com.device.manager.main.vo.reqeust_entity;

import io.swagger.annotations.ApiModel;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/8 23:37
 * @file: insertOperation
 * @description: 星期一
 */
public class InsertOperation {


    private int operationId;

    private String userName;

    private String operationInfo;

    private String submitTime;

    private int operationType;

    private int result;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getOperationInfo() {
        return operationInfo;
    }

    public void setOperationInfo(String operationInfo) {
        this.operationInfo = operationInfo;
    }

    public String getSubmitTime() {
        return submitTime;
    }

    public void setSubmitTime(String submitTime) {
        this.submitTime = submitTime;
    }

    public int getOperationType() {
        return operationType;
    }

    public void setOperationType(int operationType) {
        this.operationType = operationType;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public int getOperationId() {
        return operationId;
    }

    public void setOperationId(int operationId) {
        this.operationId = operationId;
    }
}
