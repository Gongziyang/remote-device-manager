package com.device.manager.main.vo.reqeust_entity;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/17 17:18
 * @file: CtrlReq
 * @description: 星期三
 */
public class CtrlReq {

    private Integer deviceId;
    private Integer command;

    public Integer getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    public Integer getCommand() {
        return command;
    }

    public void setCommand(Integer command) {
        this.command = command;
    }
}
