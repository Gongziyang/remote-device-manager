package com.device.manager.main.netty;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/18 14:01
 * @file: Constant
 * @description: 星期四
 */
public class Constant {

    //控制设备命令 报文的头部长度
    public static final int HEADER_SIZE = 17;

    //控制设备命令 启动设备
    public static final int START_DEVICE = 0;

    //控制设备命令 重启设备
    public static final int RESTART_DEVICE = 0;

    //控制设备命令 暂停设备
    public static final int STOP_DEVICE = 0;

    //控制设备命令 关闭设备
    public static final int CLOSE_DEVICE = 0;

    //报文命令 cmd 登陆报文
    public static final int LOGIN_MSG = 1;

    //报文命令 cmd 报警报文
    public static final int WARN_MSG = 2;

    //报文命令 cmd 控制报文
    public static final int CONTROL_MSG = 3;

    //报文命令 cmd 回复报文
    public static final int REPLY_MSG = 4;

    //报文类型 type 设备 -> 平台
    public static final int DEVICE_TO_PLATFORM = 1;

    //报文类型 type 平台 -> 设备
    public static final int PLATFORM_TO_DEVICE = 0;
}
