package com.device.manager.main.mapper;

import com.device.manager.main.vo.entity.DeviceWarning;
import com.device.manager.main.vo.reqeust_entity.QueryWarningInfoReq;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/10 14:35
 * @file: WaringInfoMapper
 * @description: 星期三
 * 处理报警信息
 */
@Mapper
public interface WaringInfoMapper {

    public int itemSize(QueryWarningInfoReq req);

    public List<DeviceWarning> queryWarningsByPageHelper(QueryWarningInfoReq req);

    public int deleteById(int id);

    public int deleteByIds(Map<String, Object> idsMap);

    public int insertWarning(DeviceWarning deviceWarning);
}
