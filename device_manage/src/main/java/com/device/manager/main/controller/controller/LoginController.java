package com.device.manager.main.controller.controller;

import com.alibaba.fastjson.JSONObject;
import com.device.manager.main.constant.CommonConstant;
import com.device.manager.main.mapper.sys_user.SysUserRoleMapper;
import com.device.manager.main.service.UserService;
import com.device.manager.main.service.sys_user.SysMenuService;
import com.device.manager.main.utils.common.CommonUtils;
import com.device.manager.main.utils.common.JwtUtil;
import com.device.manager.main.utils.common.PasswordUtil;
import com.device.manager.main.utils.common.RedisUtils;
import com.device.manager.main.vo.entity.DeviceWarning;
import com.device.manager.main.vo.msg_standard.TRequest;
import com.device.manager.main.vo.msg_standard.TResponse;
import com.device.manager.main.vo.sys_user.SysMenu;
import com.device.manager.main.vo.sys_user.SysUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/20 15:28
 * @file: LoginController
 * @description: 星期六
 */
@CrossOrigin
@RestController
@Log4j2
@RequestMapping("/test")
@Api(tags = "登录处理接口以及返回用户拥有的菜单接口")
public class LoginController {

    @Autowired
    UserService userService;

    @Autowired
    SysUserRoleMapper userRoleMapper;

    @Autowired
    SysMenuService sysMenuService;

    @Autowired
    RedisUtils redisUtils;


    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ApiOperation("登录接口，不需要任何权限，登录成功后会返回token")
    public TResponse<Object> login(@RequestBody() TRequest<SysUser> request) {
        if (request == null || request.getData() == null) {
            log.info("/sys/login", "Required params are uncorrect");
            throw new RuntimeException("请输入正确的用户名和密码");
        }
        SysUser user = request.getData();
        //校验用户是否存在
        SysUser userDb = userService.getUserByName(user.getUserName());
        System.out.println("[userDb]" + JSONObject.toJSONString(userDb));
        TResponse<Object> response = userService.checkUserIsEffective(userDb);

        if (response.getRet() == -1) {
            return response;
        }
        //检验用户密码是否正确
        String sysPass = PasswordUtil.encrypt(user.getUserName(), user.getPassWord(), userDb.getSalt());
        if (!sysPass.equals(userDb.getPassWord())) {
            response.setRet(-1);
            response.setMsg("密码错误，请重新输入");
            return response;
        }

        //检查用户帐号状态
        if (!"0".equals(userDb.getFrezzeState() + "")) {
            response.setMsg("账号已经被冻结，请联系管理员");
            response.setRet(-1);
            return response;
        }
        //派发token
        issueToken(userDb, response);
        return response;
    }

    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    @ApiOperation("登出接口，不需要任何权限，登出成功后会清除token")
    public TResponse<Object> logout(HttpServletRequest request, HttpServletResponse response) {
        TResponse<Object> tResponse = new TResponse<>();
        String token = request.getHeader(CommonConstant.ACCESS_TOKEN);
        if (CommonUtils.isEmpty(token)) {
            tResponse.setMsg("退出登录失败");
            tResponse.setRet(-1);
            return tResponse;
        }

        String userName = JwtUtil.getUsername(token);
        SysUser sysUser = userService.getUserByName(userName);
        if (sysUser != null) {
            log.info("用户[" + userName + "]" + "退出登录成功");
            //删除缓存中的token
            redisUtils.del(CommonConstant.PREFIX_USER_TOKEN + token);
            //删除缓存中的角色和权限集合
            redisUtils.del(CommonConstant.LOGIN_USER_CACHERULES_ROLE + userName);
            redisUtils.del(CommonConstant.LOGIN_USER_CACHERULES_PERMISSION + userName);

            //退出登录，该方法会清除session等信息，前后端分离的情况可以不用
            Subject subject = SecurityUtils.getSubject();
            subject.logout();

            tResponse.setMsg("退出登录成功");
            tResponse.setRet(0);
            return tResponse;
        } else {
            tResponse.setRet(-1);
            tResponse.setMsg("无效的token");
            return tResponse;
        }
    }

    @ApiOperation("用户注册接口，不需要任何权限，密码会通过加密算法得出以密文的形式保存在数据库中")
    @RequestMapping(value = "/regist", method = RequestMethod.POST)
    public TResponse<Object> regist(@RequestBody() TRequest<SysUser> userTRequest) {
        if (userTRequest == null || userTRequest.getData() == null) {
            log.info("/sys/regist", "Required params are uncorrect");
            throw new RuntimeException("请输入正确参数");
        }
        SysUser user = userTRequest.getData();
        String userName = user.getUserName();
        String passWord = user.getPassWord();
        TResponse<Object> response = new TResponse<>();
        //判断用户名是否已经存在
        SysUser userByName = userService.getUserByName(userName);
        if (userByName != null) {
            response.setMsg("用户名已存在，请换个名字试试");
            response.setRet(-1);
            return response;
        }
        try {
            //获取一个密码盐，将其转为16进制的字符串
            byte[] salt = PasswordUtil.getSalt();
            String saltStr = PasswordUtil.bytesToHexString(salt).substring(0, 8);
            //用户名+密码+密码盐通过算法得到加密字符串，登陆验证时需要使用同样的密码盐。
            String enPass = PasswordUtil.encrypt(userName, passWord, saltStr);
            user.setSalt(saltStr);
            user.setPassWord(enPass);
        } catch (Exception e) {
            e.printStackTrace();
        }

        int regist = userService.regist(user);
        //插入用户表失败，直接返回
        if (regist == 0) {
            response.setRet(-1);
            response.setMsg("注册失败");
        }
        //获取自动生成的userId
        int userId = userTRequest.getData().getId();
        //默认是是普通用户 ： 3
        int insert = userRoleMapper.insert(userId, 3);

        //插入用户角色表失败，直接返回
        if (insert == 0) {
            response.setRet(-1);
            response.setMsg("注册失败");
        }
        response.setRet(0);
        response.setMsg("注册成功");
        return response;
    }

    /**
     * @param user     从数据库中查询到的用户
     * @param response 响应结果
     *                 description : 调用JwtUtil的sign方法并将userName和passWord作为参数传入生成token,
     *                 后面shiro验证的时候可以通过token来获取用户名，从而配合数据库对用户进行
     *                 身份认证和鉴权。生成tokenh后将token存在redishu缓存中，并设置过期时间。
     **/
    public void issueToken(SysUser user, TResponse<Object> response) {

        String token = JwtUtil.sign(user.getUserName(), user.getPassWord());
        System.out.println("[issueToken]" + token);
        //将token放到redis缓存中
        redisUtils.set(CommonConstant.PREFIX_USER_TOKEN + token, token);
        //设置token过期时间
        redisUtils.expire(CommonConstant.PREFIX_USER_TOKEN + token, JwtUtil.EXPIRE_TIME / 1000);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("user", user);
        jsonObject.put("token", token);
        response.setData(jsonObject);
        response.setMsg("登录成功");
        response.setRet(0);
    }


    /**
     * swagger接口注释
     **/
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Access-Token", value = "Token令牌", required = true, dataType = "String", paramType = "header"),
    })
    /**
     * 登录过后访问菜单
     * **/
//    @RequestMapping(value = "/menu", method = RequestMethod.POST)
    @RequiresPermissions(value = {"/query/query_menu"})
    @ApiOperation("根据用户的角色查询出其拥有的菜单，返回菜单集合List,前台根据菜单构建导航栏")
    public TResponse<Object> adminMenu() {
        //拿到security中的用户信息
        SysUser sysUser = (SysUser) SecurityUtils.getSubject().getPrincipal();
        System.out.println("[Subject中拿到的用户的值]" + JSONObject.toJSONString(sysUser));
        List<SysMenu> sysMenus = sysMenuService.queryByUser(sysUser.getUserName());
        TResponse<Object> response = new TResponse<>();
        response.setRet(0);
        response.setMsg("查询成功");
        response.setData(sysMenus);
        return response;
    }


    /**
     * 获取菜单名称数组
     **/
    public List<String> getMenuNameList(List<SysMenu> menus) {
        List<String> list = new ArrayList<>(16);
        for (SysMenu menu : menus) {
            list.add(menu.getNameZh());
            if (menu.getChildren() != null) {
                list.addAll(getMenuNameList(menu.getChildren()));
            }
        }
        return list;
    }


    //=======================================================test-req-menu
    @RequestMapping(value = "/menu", method = RequestMethod.POST)
    public TResponse<Object> menu(@RequestBody() TRequest<String> request) {

        System.out.println("[userName]" + request.getData());
        List<SysMenu> yyfMenus = sysMenuService.queryByUser(request.getData());
        TResponse<Object> response = new TResponse<>();
        response.setRet(0);
        response.setMsg("查询成功");
        System.out.println("[The search result of menus]" + JSONObject.toJSONString(yyfMenus));
        response.setData(getMenuNameList(yyfMenus));
        return response;
    }


}
