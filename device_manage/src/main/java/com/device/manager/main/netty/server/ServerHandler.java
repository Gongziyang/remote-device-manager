package com.device.manager.main.netty.server;

import com.alibaba.fastjson.JSONObject;
import com.device.manager.main.utils.custom.CrcUtil;
import com.device.manager.main.netty.msg.BaseInf;
import com.device.manager.main.netty.msg.*;
import com.device.manager.main.utils.custom.BeanUtils;
import io.netty.channel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/15 23:21
 * @file: ServerHandler
 * @description: 星期一
 */
public class ServerHandler extends SimpleChannelInboundHandler<BaseInf<Object>> {

    private BizHandler bizHandler = BeanUtils.getBean(BizHandler.class);

    private static Logger logger = LoggerFactory.getLogger(ServerHandler.class);

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        System.out.println("[The device end is connected]" + ctx.channel().localAddress());
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        System.out.println("[Connection is closed]" + ctx.channel().localAddress());
        int ret = bizHandler.loginOutHandle(ctx.channel());
        if (ret == 0) {
            System.out.println("设备离线");
        }
    }


    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext,
                                BaseInf<Object> tBaseInf) throws Exception {
        String info = JSONObject.toJSONString(tBaseInf);
        logger.info("[server-BizHandler-received a pkt]", info);
        boolean result = checkCrc(tBaseInf);
        if (!result) {
            System.out.println("[crc校验失败，丢弃一个报文...]" + JSONObject.toJSONString(tBaseInf));
            return;
        }
        try {
            Object msg = tBaseInf.getMsg();
            if (msg instanceof DeviceLoginMsg) {
                //处理登录请求
                bizHandler.loginHandle(tBaseInf);
            } else if (msg instanceof WarningMsg) {
                //处理报警请求
                bizHandler.warningHandle(tBaseInf);
            } else if (msg instanceof RetMsg) {
                //处理设备回复信息
                bizHandler.replyHandle(tBaseInf);
            } else if (msg instanceof HeartBeatMsg) {
                //处理设备心跳信息
                System.out.println("[server]" + "received a heartBeat info");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        ctx.close();
        throw new RuntimeException("Occur a exception,connection is forced shutdown");
    }

    /**
     * validate crc
     **/
    public boolean checkCrc(BaseInf<Object> inf) {
        Object msg = inf.getMsg();
        byte[] bytes;
        //如果是登录报文，不要计算信道channel
        if (msg instanceof DeviceLoginMsg) {
            DeviceLoginMsg loginMsg = new DeviceLoginMsg();
            loginMsg.setDeviceId(((DeviceLoginMsg) msg).getDeviceId());
            loginMsg.setLoginTime(((DeviceLoginMsg) msg).getLoginTime());
            bytes = JSONObject.toJSONString(loginMsg).getBytes();
        } else {
            bytes = JSONObject.toJSONString(msg).getBytes();
        }
        short crc = inf.getCrc();
        return CrcUtil.checkCrc(bytes, crc);
    }

}
