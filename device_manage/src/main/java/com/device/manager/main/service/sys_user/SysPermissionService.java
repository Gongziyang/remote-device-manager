package com.device.manager.main.service.sys_user;

import com.device.manager.main.vo.sys_user.SysPermission;

import java.util.List;
import java.util.Set;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/21 13:35
 * @file: SysPermissionService
 * @description: 星期日
 */
public interface SysPermissionService {

    Set<String> getByUser(String username);
}
