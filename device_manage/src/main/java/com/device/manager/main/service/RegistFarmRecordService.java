package com.device.manager.main.service;

import com.device.manager.main.vo.entity.Farm;
import com.device.manager.main.vo.reqeust_entity.InsertRegistFarmRecord;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/8 23:55
 * @file: RegistFarmRecordService
 * @description: 星期一
 */
public interface RegistFarmRecordService {

    int insertRecord(InsertRegistFarmRecord registFarmRecord);

    Farm getFarmByOptionIdAndUserName(String userName, int operationId);

    int deleteByOptionIdAndUserName(String userName, int operationId);
}
