package com.device.manager.main.service.sys_user.impl;

import com.device.manager.main.mapper.sys_user.SysPermissionMapper;
import com.device.manager.main.service.sys_user.SysPermissionService;
import com.device.manager.main.vo.sys_user.SysPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/21 13:35
 * @file: SysPermissionServiceImpl
 * @description: 星期日
 */
@Service
public class SysPermissionServiceImpl implements SysPermissionService {

    @Autowired
    SysPermissionMapper permissionMapper;

    @Override
    public Set<String> getByUser(String username) {
        //获取出用户的所有权限
        List<SysPermission> permissions = permissionMapper.getByUser(username);
        Iterator<SysPermission> iterator = permissions.iterator();
        Set<String> urls = new HashSet<>(100);
        while (iterator.hasNext()) {
            String url = iterator.next().getUrl();
            urls.add(url);
        }
        return urls;
    }
}
