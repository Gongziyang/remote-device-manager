package com.device.manager.main.vo.reqeust_entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/9 16:33
 * @file: UpdateDeviceReq
 * @description: 边界设备和插入设备都使用该类
 */
@ApiModel(description = "包含注册设备时所需要的参数")
public class RegistDeviceReq {


    @ApiModelProperty(name = "userName", value = "用户名", dataType = "String", example = "yyf")
    private String userName;
    @ApiModelProperty(name = "type", value = "操作类型，前端默认传入[1 注册农场 0 注册设备]", dataType = "Integer", example = "2")
    private int type; //操作类型
    @ApiModelProperty(name = "deviceNumber", value = "设备编号", dataType = "String", example = "DEM25")
    private String deviceNumber;
    @ApiModelProperty(name = "deviceName", value = "设备名称", dataType = "String", example = "温度监控器")
    private String deviceName;
    @ApiModelProperty(name = "deviceType", value = "设备类型", dataType = "Integer", example = "0")
    private int deviceType;
    @ApiModelProperty(name = "deviceLng", value = "设备经度", dataType = "float", example = "32.5")
    private float deviceLng;
    @ApiModelProperty(name = "deviceLat", value = "设备纬度", dataType = "float", example = "18.3")
    private float deviceLat;
    @ApiModelProperty(name = "deviceInfo", value = "设备信息", dataType = "String", example = "{'deviceName':'温度监控器','deviceNumber':'DGH25'}")
    private String deviceInfo;
    @ApiModelProperty(name = "registTime", value = "注册时间", dataType = "String", example = "2020-09-01")
    private String registTime;

    private int farmId;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public int getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(int deviceType) {
        this.deviceType = deviceType;
    }

    public float getDeviceLng() {
        return deviceLng;
    }

    public void setDeviceLng(float deviceLng) {
        this.deviceLng = deviceLng;
    }

    public float getDeviceLat() {
        return deviceLat;
    }

    public void setDeviceLat(float deviceLat) {
        this.deviceLat = deviceLat;
    }

    public String getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(String deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public String getRegistTime() {
        return registTime;
    }

    public void setRegistTime(String registTime) {
        this.registTime = registTime;
    }

    public int getFarmId() {
        return farmId;
    }

    public void setFarmId(int farmId) {
        this.farmId = farmId;
    }
}
