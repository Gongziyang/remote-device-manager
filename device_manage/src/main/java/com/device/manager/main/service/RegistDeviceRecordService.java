package com.device.manager.main.service;

import com.device.manager.main.vo.entity.Device;
import com.device.manager.main.vo.reqeust_entity.InsertRegistDeviceRecord;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/9 17:28
 * @file: RegistDeviceRecordService
 * @description: 星期二
 */
public interface RegistDeviceRecordService {

    int insertRecord(InsertRegistDeviceRecord deviceRecord);

    Device getDeviceByOptionIdAndUserName(String userName, int operationId);

    int deleteByOptionIdAndUserName(String userName, int operationId);
}
