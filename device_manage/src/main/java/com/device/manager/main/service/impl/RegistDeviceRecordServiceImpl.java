package com.device.manager.main.service.impl;

import com.device.manager.main.mapper.RegistDeviceRecordMapper;
import com.device.manager.main.service.RegistDeviceRecordService;
import com.device.manager.main.vo.entity.Device;
import com.device.manager.main.vo.reqeust_entity.InsertRegistDeviceRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/9 17:29
 * @file: RegistDeviceRecordServiceImpl
 * @description: 星期二
 */
@Service
public class RegistDeviceRecordServiceImpl implements RegistDeviceRecordService {

    @Autowired
    RegistDeviceRecordMapper deviceRecordMapper;

    @Override
    public int insertRecord(InsertRegistDeviceRecord deviceRecord) {
        return deviceRecordMapper.insertRecord(deviceRecord);
    }

    @Override
    public Device getDeviceByOptionIdAndUserName(String userName, int operationId) {
        Map<String, Object> map = new HashMap<>(16);
        map.put("userName", userName);
        map.put("operationId", operationId);
        return deviceRecordMapper.getDeviceByOptionIdAndUserName(map);
    }

    @Override
    public int deleteByOptionIdAndUserName(String userName, int operationId) {
        Map<String, Object> map = new HashMap<>(16);
        map.put("userName", userName);
        map.put("operationId", operationId);
        return deviceRecordMapper.deleteByOptionAndUserName(map);
    }
}
