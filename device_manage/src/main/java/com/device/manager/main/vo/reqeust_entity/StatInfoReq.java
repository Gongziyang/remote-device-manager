package com.device.manager.main.vo.reqeust_entity;

/**
 * @author: yyf
 * @create: 2021 04 2021/4/18 15:41
 * @file: StatInfoReq
 * @description: 星期日
 */
public class StatInfoReq {

    int farmId = -1;

    String userName = null;

    public int getFarmId() {
        return farmId;
    }

    public void setFarmId(int farmId) {
        this.farmId = farmId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
