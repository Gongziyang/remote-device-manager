package com.device.manager.main.vo.sys_user;

import java.io.Serializable;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/19 9:48
 * @file: SysUser
 * @description: 星期五
 */
public class SysUser implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private int id;

    /**
     * 登录账号
     */
    private String userName = null;

    /**
     * 真实姓名
     */
    private String realName = null;

    /**
     * 密码
     */
    private String passWord = null;

    /**
     * md5密码盐
     */
    private String salt = null;

    /**
     * 冻结状态（0，正常，1已删除）
     */
    private int frezzeState = 0;


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFrezzeState() {
        return frezzeState;
    }

    public void setFrezzeState(int frezzeState) {
        this.frezzeState = frezzeState;
    }
}
