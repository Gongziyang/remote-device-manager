package com.device.manager.main.service;

import com.device.manager.main.vo.msg_standard.TResponse;
import com.device.manager.main.vo.reqeust_entity.BaseResp;
import com.device.manager.main.vo.reqeust_entity.QueryUserReq;
import com.device.manager.main.vo.sys_user.SysUser;

import java.util.Set;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/11 15:36
 * @file: UserService
 * @description: 星期四
 */
public interface UserService {

    int itemSize();

    TResponse<BaseResp> getUserByPageHelper(QueryUserReq req);

    TResponse<Integer> deleteUserByUserName(String userName);

    TResponse<Integer> deleteUserByUserNames(String[] userNames);

    TResponse<Integer> updateUser(SysUser user);

    SysUser getUserByName(String username);

    /**
     * 通过用户名获取用户角色集合
     *
     * @param username 用户名
     * @return 角色集合
     */
    Set<String> getUserRolesSet(String username);

    /**
     * 通过用户名获取用户权限集合
     *
     * @param username 用户名
     * @return 权限集合
     */
    Set<String> getUserPermissionsSet(String username);


    /**
     * 注册用户
     **/
    int regist(SysUser user);

    /**
     * 校验用户是否有效
     *
     * @param sysUser
     * @return
     */
    TResponse<Object> checkUserIsEffective(SysUser sysUser);
}
