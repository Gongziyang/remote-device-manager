package com.device.manager.main.vo.sys_user;

import java.io.Serializable;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/19 9:49
 * @file: SysUserRole
 * @description: 星期五
 */
public class SysUserRole implements Serializable {
    private static final long serialVersionUID = 1L;


    private int id;

    /**
     * 用户id
     */
    private int userId;

    /**
     * 角色id
     */
    private int roleId;

    public SysUserRole() {
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public SysUserRole(int userId, int roleId) {
        this.userId = userId;
        this.roleId = roleId;
    }
}
