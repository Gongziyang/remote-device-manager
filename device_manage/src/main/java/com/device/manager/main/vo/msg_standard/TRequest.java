package com.device.manager.main.vo.msg_standard;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/7 22:59
 * @file: TRequest
 * @description: 星期日
 */
public class TRequest<T> implements Serializable {


    @JsonProperty
    private String TimeStamp;  //时间戳


    @JsonProperty
    private T Data;  //request请求数据


    public String getTimeStamp() {
        return TimeStamp;
    }


    public void setTimeStamp(String timeStamp) {
        TimeStamp = timeStamp;
    }


    public T getData() {
        return Data;
    }


    public void setData(T data) {
        Data = data;
    }

}
