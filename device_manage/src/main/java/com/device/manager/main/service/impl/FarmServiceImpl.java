package com.device.manager.main.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.device.manager.main.mapper.FarmMapper;
import com.device.manager.main.service.*;
import com.device.manager.main.utils.custom.ResponseUtils;
import com.device.manager.main.vo.entity.Farm;
import com.device.manager.main.vo.msg_standard.TResponse;
import com.device.manager.main.vo.reqeust_entity.*;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/8 17:36
 * @file: FarmServiceImpl
 * @description: 星期一
 */
@Service
public class FarmServiceImpl implements FarmService {

    @Autowired
    FarmMapper farmMapper;

    @Autowired
    UserOperationService userOperationService;

    @Autowired
    RegistFarmRecordService registFarmRecordService;

    @Autowired
    CheckApplyService checkApplyService;

    @Autowired
    DeviceService deviceService;


    @Override
    public int itemSize(Map<String, Object> map) {
        return farmMapper.itemSize(map);
    }

    @Override
    public TResponse<BaseResp> getFarmByPageHelper(QueryFarmReq req) {
        int pageNo = req.getPageNo();
        int pageSize = req.getPageSize();
        //设置分页查询
        PageHelper.startPage(pageNo, pageSize);
        TResponse<BaseResp> response = new TResponse<>();
        List<Farm> farmByPageHelper = farmMapper.getFarmByPageHelper(req);
        Map<String, Object> map = new HashMap<>();
        map.put("farmName", req.getFarmName());
        map.put("farmNumber", req.getFarmNumber());
        map.put("userName", req.getUserName());
        map.put("type", req.getType());
        map.put("registTime", req.getRegistTime());
        int size = itemSize(map);
        System.out.println("[req-itemSize]"+JSONObject.toJSONString(map));
        System.out.println("[size]"+size);
        int pageCount = size % req.getPageSize() == 0 ? size / req.getPageSize() : (size / req.getPageSize() + 1);
        if (pageNo > pageCount) {
            ResponseUtils.set(response, -1, "暂无农场信息", null);
        } else {
            BaseResp ret = new BaseResp();
            ret.setList(farmByPageHelper);
            ret.setItemSize(size);
            ret.setPageNo(pageNo);
            ret.setPageCount(pageCount);
            ResponseUtils.set(response, 0, "查询成功", ret);
        }
        String timeStamp = getTimeStamp();
        response.setTimeStamp(timeStamp);
        return response;
    }

    /**
     * 通过农场ID来删除农场
     * 1、删除农场表中的记录
     * 2、删除设备表中关联该农场的设备
     **/
    @Override
    public TResponse<Integer> deleteFarmById(int id) {
        //1、删除农场的设备
        int affects0 = deviceService.deleteDevicesByFarmId(id);
        int affects1 = farmMapper.deleteFarmById(id);
        TResponse<Integer> response = new TResponse<>();
        String timeStamp = getTimeStamp();
        //如果删除时出现错误
        if (affects0 + affects1 <= 1) {
            ResponseUtils.set(response, -1, "删除失败", -1);
        } else {
            ResponseUtils.set(response, 0, "一共删除了：" + affects0 + affects1 + "条记录", affects0 + affects1);
        }
        response.setTimeStamp(timeStamp);
        return response;
    }


    /**
     * 1、向user_operation里面插入相关数据，返回自增id
     * 2、向regist_farm_record里面插入数据
     * 3、向用户审核记录表插入数据，等待管理员审核
     **/
    @Override
    public TResponse<Integer> registFarm(RegistFarmReq req) {
        //1、向用户操作表里面插入，获得操作id
        InsertOperation operation = new InsertOperation();
        operation.setUserName(req.getUserName());
        operation.setOperationInfo(JSONObject.toJSONString(req));
        operation.setOperationType(req.getType());
        operation.setSubmitTime(req.getRegistTime());
        //默认是1：正在审核中
        operation.setResult(1);
        userOperationService.insertOperation(operation);
        int operationId = operation.getOperationId();
        //2、向用户注册农场表里面插入
        InsertRegistFarmRecord registFarmRecord = new InsertRegistFarmRecord();
        registFarmRecord.setUserName(req.getUserName());
        registFarmRecord.setOperationId(operationId);
        registFarmRecord.setFarmNumber(req.getFarmNumber());
        registFarmRecord.setFarmName(req.getFarmName());
        registFarmRecord.setFarmLat(req.getLat());
        registFarmRecord.setFarmLng(req.getLng());
        registFarmRecord.setFarmStatus(req.getFarmStatus());
        registFarmRecord.setFarmType(req.getFarmType());
        registFarmRecord.setFarmInfo(req.getFarmInfo());
        registFarmRecord.setRegistTime(getTimeStamp());
        registFarmRecord.setUserName(req.getUserName());
        int result0 = registFarmRecordService.insertRecord(registFarmRecord);

        //3、向审核记录表里面插入数据
        InsertCheckApply checkApply = new InsertCheckApply();
        checkApply.setOperationId(operationId);
        checkApply.setApplyTime(req.getRegistTime());
        checkApply.setApplyType(req.getType());
        checkApply.setApplyInfo("用户申请注册农场");
        checkApply.setStatus(1);
        checkApply.setUserName(req.getUserName());

        int result1 = checkApplyService.insertApply(checkApply);

        TResponse<Integer> response = new TResponse<>();
        String timeStamp = getTimeStamp();
        if (result0 + result1 <= 1) {
            ResponseUtils.set(response, -1, "操作失败", -1);
        } else {
            ResponseUtils.set(response, 0, "操作成功，请等待管理员审核", result0 + result1);
        }
        response.setTimeStamp(timeStamp);
        return response;
    }

    /**
     * 1、删除农场
     * 2、删除农场关联设备
     * 3、删除农场——用户关联表中的记录
     * 4、删除设备表里面的设备
     **/
    @Override
    public TResponse<Integer> deleteFarmByIds(int[] ids) {
        //封装成map类型
        Map<String, Object> idsMap = new HashMap<>(16);
        idsMap.put("ids", ids);
        int affects0 = farmMapper.deleteFarmByIds(idsMap);
        int affects1 = deviceService.deleteDevicesByFarmIds(ids);
        int total = affects0 + affects1;
        TResponse<Integer> response = new TResponse<>();
        String timeStamp = getTimeStamp();
        if (total == 0) {
            ResponseUtils.set(response, -1, "批量删除失败", -1);
        } else {
            ResponseUtils.set(response, 0, "操作成功，删除：" + total + "条记录", total);
        }
        response.setTimeStamp(timeStamp);
        return response;
    }

    @Override
    public TResponse<Integer> updateFarm(Farm farm) {
        int result = farmMapper.updateFarm(farm);
        TResponse<Integer> response = new TResponse<>();
        String timeStamp = getTimeStamp();
        if (result == 0) {
            ResponseUtils.set(response, -1, "更新信息失败", -1);
        } else {
            ResponseUtils.set(response, 0, "信息更新成功", result);
        }
        response.setTimeStamp(timeStamp);
        return response;
    }

    @Override
    public int deleteFarmByUserName(String userName) {
        return farmMapper.deleteFarmByUserName(userName);
    }

    @Override
    public int deleteFarmByUserNames(String[] userNames) {
        Map<String, Object> map = new HashMap<>(100);
        map.put("userNames", map);
        return farmMapper.deleteFarmByUserNames(map);
    }


    /**
     * 管理员同意用户的注册记录时，将农场信息插入到农场表中
     **/
    @Override
    public int insertFarm(Farm farm) {
        return farmMapper.insertFarm(farm);
    }

    private String getTimeStamp() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String timeStamp = dateFormat.format(System.currentTimeMillis());
        return timeStamp;
    }
}
