package com.device.manager.main.netty.msg;

import io.netty.channel.Channel;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/16 14:29
 * @file: DeviceLoginMsg
 * @description: 星期二
 */
public class DeviceLoginMsg {

    public int deviceId;

    public String loginTime;

    public Channel channel;//信道

    public int getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }

    public String getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(String loginTime) {
        this.loginTime = loginTime;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }
}
