package com.device.manager.main.service.sys_user.impl;

import com.device.manager.main.mapper.sys_user.SysMenuMapper;
import com.device.manager.main.service.sys_user.SysMenuService;
import com.device.manager.main.vo.sys_user.SysMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/21 9:15
 * @file: SysPermissionServiceImpl
 * @description: 星期日
 */
@Service
public class SysMenuServiceImpl implements SysMenuService {

    @Autowired
    SysMenuMapper sysMenuMapper;

    @Override
    public List<SysMenu> queryByUser(String username) {
        List<SysMenu> sysMenus = sysMenuMapper.queryByUser(username);
        List<SysMenu> permTree = buildMenuTree(sysMenus);
        return permTree;
    }

    /**
     * 构建菜单树
     **/
    private List<SysMenu> buildMenuTree(List<SysMenu> permissions) {
        //找到所有的子菜单
        for (SysMenu permission : permissions) {
            getChild(permission, permissions);
        }
        //只保留最上层的菜单
        Iterator<SysMenu> iterator = permissions.iterator();
        while (iterator.hasNext()) {
            SysMenu p = iterator.next();
            //移除不是顶级菜单的项
            if (p.getParentId() != 0) {
                iterator.remove();
            }
        }
        return permissions;
    }

    public void getChild(SysMenu permission, List<SysMenu> permissions) {
        int id = permission.getId();
        for (SysMenu p : permissions) {
            if (p.getParentId() == id) {
                permission.getChildren().add(p);
            }
        }
    }
}
