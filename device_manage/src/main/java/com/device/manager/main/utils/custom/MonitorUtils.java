package com.device.manager.main.utils.custom;

import com.alibaba.fastjson.JSONObject;
import org.hyperic.sigar.ProcState;
import org.hyperic.sigar.Sigar;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ：YYF
 * @date ：Created in 2021/3/12 11:03
 * @description：
 */
public class MonitorUtils {

    /**
     * oldSysCpuSysTime : 上一个状态CPU的内核运行时间
     * oldSysCpuUsrTime : 上一个状态CPU的用户运行时间
     * oldSysCpuIdleTime : 上一个状态CPU的空闲时间
     * oldProcCpuSysTime : 上一个状态进程CPU的内核运行时间
     * oldProcCpuUsrTime : 上一个状态进程CPU的用户运行时间
     */
    long oldSysCpuSysTime;
    long oldSysCpuUsrTime;
    long oldSysCpuIdleTime;
    long oldProcCpuSysTime;
    long oldProcCpuUsrTime;

    /**
     * info : 保存服务器的相关信息
     **/
    Map<String, Object> info;

    public Map<String, Object> getInfo() {
        return info;
    }

    public void setInfo(Map<String, Object> info) {
        this.info = info;
    }

    /**
     * Sigar模块
     **/
    Sigar sigar;

    {
        sigar = new Sigar();
        info = new HashMap<String, Object>();
        try {
            oldProcCpuSysTime = sigar.getProcCpu(sigar.getPid()).getSys();
            oldProcCpuUsrTime = sigar.getProcCpu(sigar.getPid()).getUser();
            oldSysCpuUsrTime = sigar.getCpu().getUser();
            oldSysCpuSysTime = sigar.getCpu().getSys();
            oldSysCpuIdleTime = sigar.getCpu().getIdle();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取本机内存使用情况
     * String:信息名称
     * object:对应值
     */
    public void getSysMemInfo(Map<String, Object> map) {
        try {
            double sysMemUsed = (double) sigar.getMem().getUsed() / 1024 / 1024;
            double sysMemAvailable = (double) sigar.getMem().getFree() / 1024 / 1024;
            double sysMemUsedRate = sysMemUsed / ((double) sigar.getMem().getTotal() / 1024 / 1024);
            map.put("sysMemUsed", format(sysMemUsed, "MB"));
            map.put("sysMemAvailable", format(sysMemAvailable, "MB"));
            map.put("sysMemUsedRate", format(sysMemUsedRate, "%"));
        } catch (Exception e) {
            System.out.println("获取本机内存信息出错");
            e.printStackTrace();
        }
    }

    /**
     * 获取本机CPU使用情况
     */
    public void getSysCpuInfo(Map<String, Object> map) {

        try {
            double sysCpuUsedRate = sigar.getCpuPerc().getCombined();
            map.put("sysCpuUsedRate", format(sysCpuUsedRate, "%"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取进程内存使用情况
     *
     * @param pid : 进程ID
     */
    public void getProcMemInfo(long pid, Map<String, Object> map) {
        try {
            double procMemUsed = (double) sigar.getProcMem(pid).getSize() / 1024 / 1024;
            double procMemUsedRate = procMemUsed / ((double) sigar.getMem().getTotal() / 1024 / 1024);
            map.put("procMemUsed", format(procMemUsed, "MB"));
            map.put("procMemUsedRate", format(procMemUsedRate, "%"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取进程cpu占用率
     *
     * @param pid
     */
    public void getProcCpuInfo(long pid, Map<String, Object> map) {
        try {

            long nowProcCpuSysTime = sigar.getProcCpu(pid).getSys();
            long nowProcCpuUsrTime = sigar.getProcCpu(pid).getUser();
            long sysCpuRuntime = (sigar.getCpu().getSys() + sigar.getCpu().getUser() + sigar.getCpu().getIdle()) - (oldSysCpuUsrTime + oldSysCpuSysTime + oldSysCpuIdleTime);
            double procCpuUsedRate = (double) ((nowProcCpuSysTime + nowProcCpuUsrTime) - (oldProcCpuSysTime + oldProcCpuUsrTime)) / (double) sysCpuRuntime;
            map.put("procCpuUsedRate", format(procCpuUsedRate, "%"));
            oldSysCpuIdleTime = sigar.getCpu().getIdle();
            oldSysCpuSysTime = sigar.getCpu().getSys();
            oldSysCpuUsrTime = sigar.getCpu().getUser();
            oldProcCpuUsrTime = nowProcCpuUsrTime;
            oldProcCpuSysTime = nowProcCpuSysTime;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getProcRunState(long pid, Map<String, Object> map) {
        try {
            ProcState procState = sigar.getProcState(pid);
            if (procState.getState() == 'R') {
                map.put("procRunState", "运行中");
            } else if (procState.getState() == 'T') {
                map.put("procRunState", "已停止");
            } else if (procState.getState() == 'S') {
                map.put("procRunState", "阻塞中");
            } else if (procState.getState() == 'D') {
                map.put("procRunState", "空闲中");
            }
        } catch (Exception e) {
            map.put("procRunState", "已停止");
        }
    }

    /**
     * 格式转换
     **/
    public String format(double ret, String type) {
        String str = String.valueOf(ret);
        if (type.equals("MB")) {
            if (Double.isNaN(ret)) {
                return "0.0MB";
            }
            if (str.indexOf(".") != -1) {
                String fix[] = str.split("\\.");
                int prefix = fix[0].length();
                int suffix = fix[1].length();
                if (suffix > 1) {
                    str = str.substring(0, prefix + 2);
                }
            }
            return str + "MB";
        }
        if (type.equals("%")) {
            if (Double.isNaN(ret)) {
                return "0.0%";
            }
            if (str.length() > 4) {
                str = str.substring(0, 4);
            }
            return Double.valueOf(str) * 100 + "%";
        }
        return null;
    }

    public String start() {
        long pid = sigar.getPid();
        getSysCpuInfo(getInfo());
        getSysMemInfo(getInfo());
        getProcCpuInfo(pid, getInfo());
        getProcMemInfo(pid, getInfo());
        getProcRunState(pid, getInfo());
        Map<String, Object> info = getInfo();
        info.put("pid", pid);
        return JSONObject.toJSONString(info);
    }

}
