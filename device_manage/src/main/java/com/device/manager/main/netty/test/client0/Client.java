package com.device.manager.main.netty.test.client0;

import com.device.manager.main.netty.Decoder;
import com.device.manager.main.netty.Encoder;
import com.device.manager.main.netty.PackageSpilter;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.net.InetSocketAddress;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/15 23:37
 * @file: NettyClient
 * @description: 星期一
 */
public class Client {

    private BizHandler bizHandler = new BizHandler();

    public void connect() {

        Bootstrap bootstrap = new Bootstrap();
        EventLoopGroup worker = new NioEventLoopGroup();
        bootstrap.group(worker);
        bootstrap.channel(NioSocketChannel.class);
        bootstrap.handler(new ChannelInitializer<SocketChannel>() {
            @Override
            protected void initChannel(SocketChannel socketChannel) throws Exception {
//              socketChannel.pipeline().addLast(new IdleStateHandler(60, 0, 0, TimeUnit.SECONDS));
                socketChannel.pipeline().addLast(new PackageSpilter());
                socketChannel.pipeline().addLast(new Encoder());
                socketChannel.pipeline().addLast(new Decoder());
                socketChannel.pipeline().addLast(new ClientHandler());
            }
        });
        bootstrap.option(ChannelOption.SO_KEEPALIVE, true);
        try {
            ChannelFuture future = bootstrap.connect(new InetSocketAddress("localhost", 8888)).sync();
            future.addListener(new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture future) throws Exception {
                    System.out.println("成功连接服务器");

                    bizHandler.sendLogin(future.channel());

                    bizHandler.sendWarning(future.channel());
                }
            });
            //等待服务器监听端口关闭
            future.channel().closeFuture().sync();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            worker.shutdownGracefully();
        }
    }
}
