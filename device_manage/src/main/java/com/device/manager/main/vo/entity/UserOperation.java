package com.device.manager.main.vo.entity;

import java.util.Date;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/8 16:28
 * @file: UserOperation
 * @description: 星期一
 */
public class UserOperation {

    private int operationId; //用户操作记录ID

    private String userName; //关联用户名

    private String operationInfo; //操作信息

    private int operationType; //操作类型

    private String submitTime; //提交时间

    private int result; //管理员审核结果

    public int getOperationId() {
        return operationId;
    }

    public void setOperationId(int operationId) {
        this.operationId = operationId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getOperationInfo() {
        return operationInfo;
    }

    public void setOperationInfo(String operationInfo) {
        this.operationInfo = operationInfo;
    }

    public int getOperationType() {
        return operationType;
    }

    public void setOperationType(int operationType) {
        this.operationType = operationType;
    }

    public String getSubmitTime() {
        return submitTime;
    }

    public void setSubmitTime(String submitTime) {
        this.submitTime = submitTime;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }
}
