package com.device.manager.main.service;

import com.device.manager.main.vo.msg_standard.TResponse;
import com.device.manager.main.vo.reqeust_entity.BaseResp;
import com.device.manager.main.vo.reqeust_entity.InsertOperation;
import com.device.manager.main.vo.reqeust_entity.QueryOperationReq;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/8 23:40
 * @file: UserOperationService
 * @description: 星期一
 */
public interface UserOperationService {

    int insertOperation(InsertOperation operation);

    TResponse<BaseResp> queryOperationByPageHelper(QueryOperationReq operationReq);

    int itemSize(String userName);

    TResponse<Integer> deleteById(int id);

    TResponse<Integer> deleteByIds(int[] ids);

    int updateStatus(String userName, int operationId, int status);
}
