package com.device.manager.main.mapper;

import com.device.manager.main.vo.entity.Device;
import com.device.manager.main.vo.reqeust_entity.InsertRegistDeviceRecord;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/9 17:27
 * @file: RegistDeviceRecordMapper
 * @description: 星期二
 */
@Mapper
public interface RegistDeviceRecordMapper {

    public int insertRecord(InsertRegistDeviceRecord deviceRecord);

    public Device getDeviceByOptionIdAndUserName(Map<String, Object> map);

    public int deleteByOptionAndUserName(Map<String, Object> map);
}
