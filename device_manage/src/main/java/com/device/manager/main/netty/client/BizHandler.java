package com.device.manager.main.netty.client;

import com.alibaba.fastjson.JSONObject;
import com.device.manager.main.netty.msg.*;
import com.device.manager.main.vo.entity.Device;
import io.netty.channel.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/18 10:48
 * @file: BizHandler
 * @description: 星期四
 */
public class BizHandler {


    private Logger logger = LoggerFactory.getLogger(BizHandler.class);

    /**
     * @param baseInf :收到的回复报文
     *                description:收到服务端的回复报文
     **/
    public void relpyHandle(BaseInf<Object> baseInf) {
        logger.info("Client-received a relpy info", JSONObject.toJSONString(baseInf));
        int seq = baseInf.getSeq();
        String seqStr = String.valueOf(seq);
        int seqType = Integer.parseInt(seqStr.substring(0, 1));
        switch (seqType) {
            case 1:
                logger.info("Client-BizHandle-case-1:{}", "[温度检测器]这是一个登录回复报文，服务端已收到登录信息");
                break;
            case 2:
                logger.info("Client-BizHandle-case-2:{}", "[温度检测器]这是一个警报回复报文，服务端已收到警报信息");
                break;
        }
        System.out.println("---------------------------------------\n");
    }

    /**
     * @param baseInf :收到的控制报文
     **/
    public void ctrlHandle(BaseInf<Object> baseInf, Channel channel) {
        logger.info("Client-BizHandle-received a pkt:{}", "[温度检测器]收到一个控制设备报文，正在处理");
        CtrlMsg ctrlMsg = (CtrlMsg) baseInf.getMsg();
        int cmd = ctrlMsg.getCmd();
        try {
            switch (cmd) {
                case 0:
                    Thread.sleep(2500);
                    logger.info("Client-BizHandle-case-0:{}", "设备[温度检测器]收到一个启动命令，启动中...");
                    reply(baseInf, channel);
                    /** underlying code **/
                    break;
                case 1:
                    logger.info("Client-BizHandle-case-0:{}", "设备[温度检测器]收到一个重启命令，重新启动中...");
                    Thread.sleep(2000);
                    reply(baseInf, channel);
                    /** underlying code **/
                    break;
                case 2:
                    Thread.sleep(1000);
                    logger.info("Client-BizHandle-case-0:{}", "设备[温度检测器]收到一个暂停命令，暂停中...");
                    reply(baseInf, channel);
                    /** underlying code **/
                    break;
                case 3:
                    Thread.sleep(1500);
                    logger.info("Client-BizHandle-case-0:{}", "设备[温度检测器]收到一个关闭命令，关闭中...");
                    reply(baseInf, channel);
                    /** underlying code **/
                    break;
                default:
                    break;
            }
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    /**================================================================**/

    /**
     * @param channel : 对应的信道
     *                description : 发送登录报文
     **/
    public void sendLogin(Channel channel, Device device) {
        BaseInf<DeviceLoginMsg> baseInf = new BaseInf<>();
        baseInf.setVersion((short) 10);
        baseInf.setCmd((byte) 1);
        DeviceLoginMsg loginMsg = new DeviceLoginMsg();
        loginMsg.setDeviceId(device.getDeviceId());
        loginMsg.setLoginTime("2020-09-03");
        baseInf.setMsg(loginMsg);
        baseInf.setSeq(10000 + device.getDeviceId());
        baseInf.setType((short) 1);
        baseInf.setDeviceId(device.getDeviceId());
        channel.writeAndFlush(baseInf);
        logger.info("Client-BizHandle-received a pkt:{}", "[设备+" + device.getDeviceNumber() + "]" + " send a login pkt");
        System.out.println("+++++++++++++++++++++++++++++++++++++++\n");
    }

    /**
     * @param channel : 对应的信道
     *                description : 发送报警报文
     **/
    public void sendWarning(Channel channel, Device device) {
        BaseInf<WarningMsg> baseInf = new BaseInf<>();
        baseInf.setVersion((short) 10);
        baseInf.setCmd((byte) 2);
        WarningMsg warningMsg = new WarningMsg();
        warningMsg.setFarmId(device.getFarmId());
        warningMsg.setWarningInfo("[位于" + "[北纬：" + device.getDeviceLng() + ",东经：" + device.getDeviceLat() + "]" + "的设备：" + device.getDeviceName() + "发来一条报警消息：" + "]" + device.getWarningInfo());
        warningMsg.setWarningName(device.getDeviceName());
        baseInf.setMsg(warningMsg);
        baseInf.setSeq(20000 + device.getDeviceId());
        baseInf.setType((short) 1);
        baseInf.setDeviceId(device.getDeviceId());
        channel.writeAndFlush(baseInf);
        logger.info("Client-BizHandle-send a pkt:{}", "[设备+" + device.getDeviceNumber() + "]" + " send a warning pkt");
        System.out.println("+++++++++++++++++++++++++++++++++++++++\n");
    }

    public void reply(BaseInf<Object> baseInf, Channel channel) {
        System.out.println("---------------------------------------\n");
        RetMsg retMsg = new RetMsg();
        retMsg.setResult(0);
        retMsg.setInfo("操作成功");
        //回复
        baseInf.setType((short) 1);
        baseInf.setCmd((byte) 4);
        baseInf.setMsg(retMsg);
        channel.writeAndFlush(baseInf);
        logger.info("Client-BizHandle-received a pkt:{}", "[设备+" + baseInf.getDeviceId() + "]" + " send a reply pkt");
        System.out.println("+++++++++++++++++++++++++++++++++++++++\n");
    }
}
