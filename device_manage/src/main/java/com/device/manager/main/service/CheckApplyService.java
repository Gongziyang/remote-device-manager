package com.device.manager.main.service;

import com.device.manager.main.vo.msg_standard.TResponse;
import com.device.manager.main.vo.reqeust_entity.BaseResp;
import com.device.manager.main.vo.reqeust_entity.InsertCheckApply;
import com.device.manager.main.vo.reqeust_entity.QueryApplicationsReq;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/9 0:08
 * @file: CheckApplyService
 * @description: 星期二
 */
public interface CheckApplyService {

    int insertApply(InsertCheckApply insertCheckApply);

    TResponse<BaseResp> getApplicationsInfoByPageHelper(QueryApplicationsReq req);

    TResponse<Integer> deleteById(int applyId);

    TResponse<Integer> deleteByIds(int[] ids);

    TResponse<Integer> agreeApply(String userName, int operationId, int applyType);

    TResponse<Integer> refusedApply(String userName, int operationId, int applyType);

    int updateStatus(String userName, int operationId, int status);

}
