package com.device.manager.main.redis;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/15 22:47
 * @file: MessageConstant
 * @description: 星期一
 */
public class MessageConstant {

    //发布主题
    public static final String TOPIC = "deviceWarning";
}
