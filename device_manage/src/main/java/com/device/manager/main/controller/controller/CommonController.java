package com.device.manager.main.controller.controller;

import com.device.manager.main.mapper.DeviceMapper;
import com.device.manager.main.mapper.FarmMapper;
import com.device.manager.main.service.*;
import com.device.manager.main.utils.common.ParamCheckUtil;
import com.device.manager.main.vo.entity.Device;
import com.device.manager.main.vo.entity.Farm;
import com.device.manager.main.vo.reqeust_entity.StatInfoResp;
import com.device.manager.main.vo.msg_standard.TRequest;
import com.device.manager.main.vo.msg_standard.TResponse;
import com.device.manager.main.vo.reqeust_entity.*;
import com.device.manager.main.vo.sys_user.SysUser;
import com.google.gson.Gson;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/11 16:55
 * @file: AdminUserController
 * @description: 星期四
 */
@CrossOrigin
@RestController
@Api(tags = "管理员管理用户接口")
public class CommonController {


    @Autowired
    Gson gson;

    @Autowired
    WarningInfoService warningInfoService;

    @Autowired
    UserService userService;

    @Autowired
    FarmService farmService;

    @Autowired
    DeviceService deviceService;

    @Autowired
    CheckApplyService checkApplyService;

    @Autowired
    UserOperationService userOperationService;

    public static final Logger logger = LoggerFactory.getLogger(CommonController.class);


    @RequestMapping(value = "/admin/query/query_users_info", method = RequestMethod.POST)
    public TResponse<BaseResp> queryUsersInfo(@RequestBody() TRequest<QueryUserReq> reqTRequest) {
        ParamCheckUtil.handlerError(reqTRequest);
        TResponse<BaseResp> response = userService.getUserByPageHelper(reqTRequest.getData());
        return response;
    }


    @RequestMapping(value = "/admin/update/delete_user_info", method = RequestMethod.POST)
    public TResponse<Integer> deleteUserById(@RequestBody() TRequest<String> request) {
        ParamCheckUtil.handlerError(request);
        TResponse<Integer> response = userService.deleteUserByUserName(request.getData());
        return response;
    }


    @RequestMapping(value = "/admin/update/delete_users_info", method = RequestMethod.POST)
    public TResponse<Integer> deleteUserByIds(@RequestBody() TRequest<String[]> request) {
        ParamCheckUtil.handlerError(request);
        TResponse<Integer> response = userService.deleteUserByUserNames(request.getData());
        return response;
    }

    @RequestMapping(value = "/admin/update/update_user_info", method = RequestMethod.POST)
    public TResponse<Integer> updateUser(@RequestBody() TRequest<SysUser> request) {
        ParamCheckUtil.handlerError(request);
        TResponse<Integer> response = userService.updateUser(request.getData());
        return response;
    }



    @RequestMapping(value = "/user/query/query_warnings_info", method = RequestMethod.POST)
    public TResponse<BaseResp> queryWarningInfo(@RequestBody() TRequest<QueryWarningInfoReq> reqTRequest) {
        ParamCheckUtil.handlerError(reqTRequest);
        TResponse<BaseResp> response = warningInfoService.queryWarningsInfoByPageHelper(reqTRequest.getData());
        return response;
    }

    @RequestMapping(value = "/user/update/delete_warning_info", method = RequestMethod.POST)
    public TResponse<Integer> deleteWarningById(@RequestBody() TRequest<Integer> request) {
        ParamCheckUtil.handlerError(request);
        TResponse<Integer> response = warningInfoService.deleteById(request.getData());
        return response;
    }

    @RequestMapping(value = "/user/update/delete_warnings_info", method = RequestMethod.POST)
    public TResponse<Integer> deleteWarningByIds(@RequestBody() TRequest<int[]> request) {
        ParamCheckUtil.handlerError(request);
        TResponse<Integer> response = warningInfoService.deleteByIds(request.getData());
        return response;
    }



    @RequestMapping(value = "/user/query/query_operations_info", method = RequestMethod.POST)
    public TResponse<BaseResp> queryOperationInfo(@RequestBody() TRequest<QueryOperationReq> request) {
        ParamCheckUtil.handlerError(request);
        TResponse<BaseResp> response = userOperationService.queryOperationByPageHelper(request.getData());
        return response;
    }

    @RequestMapping(value = "/user/update/delete_operation_info", method = RequestMethod.POST)
    public TResponse<Integer> deleteOperationById(@RequestBody() TRequest<Integer> request) {
        ParamCheckUtil.handlerError(request);
        TResponse<Integer> response = userOperationService.deleteById(request.getData());
        return response;
    }

    @RequestMapping(value = "/user/update/delete_operations_info", method = RequestMethod.POST)
    public TResponse<Integer> deleteOperationByIds(@RequestBody() TRequest<int[]> request) {
        ParamCheckUtil.handlerError(request);
        TResponse<Integer> response = userOperationService.deleteByIds(request.getData());
        return response;
    }


    @RequestMapping(value = "/user/query/query_farms_info", method = RequestMethod.POST)
    public TResponse<BaseResp> queryFarmsInfo(@RequestBody() TRequest<QueryFarmReq> reqTRequest) {
        ParamCheckUtil.handlerError(reqTRequest);
        TResponse<BaseResp> response = farmService.getFarmByPageHelper(reqTRequest.getData());
        return response;
    }

    @RequestMapping(value = "/user/update/delete_farm_info", method = RequestMethod.POST)
    public TResponse<Integer> deleteFarmById(@RequestBody() TRequest<Integer> request) {
        ParamCheckUtil.handlerError(request);
        TResponse<Integer> response = farmService.deleteFarmById(request.getData());
        return response;
    }



    @RequestMapping(value = "/user/update/delete_farms_info", method = RequestMethod.POST)
    public TResponse<Integer> deleteFarmByIds(@RequestBody() TRequest<int[]> request) {
        ParamCheckUtil.handlerError(request);
        TResponse<Integer> response = farmService.deleteFarmByIds(request.getData());
        return response;
    }


    @RequestMapping(value = "/user/update/regist_farm_info", method = RequestMethod.POST)
    public TResponse<Integer> registFarmInfo(@RequestBody() TRequest<RegistFarmReq> registFarmReqTRequest) {
        ParamCheckUtil.handlerError(registFarmReqTRequest);
        TResponse<Integer> response = farmService.registFarm(registFarmReqTRequest.getData());
        return response;
    }

    @RequestMapping(value =
            "/user/update/update_farm_info", method = RequestMethod.POST)
    public TResponse<Integer> modifyFarmInfo(@RequestBody() TRequest<Farm> request) {
        ParamCheckUtil.handlerError(request);
        TResponse<Integer> response = farmService.updateFarm(request.getData());
        return response;
    }


    @RequestMapping(value = "/user/query/query_devices_info", method = RequestMethod.POST)
    public TResponse<BaseResp> queryDevicesInfo(@RequestBody() TRequest<QueryDeviceReq> reqTRequest) {
        ParamCheckUtil.handlerError(reqTRequest);
        TResponse<BaseResp> response = deviceService.getDevicesByPageHelper(reqTRequest.getData());
        return response;
    }


    @RequestMapping(value = "/user/update/delete_device_info", method = RequestMethod.POST)
    public TResponse<Integer> deleteDeviceById(@RequestBody() TRequest<Integer> request) {
        ParamCheckUtil.handlerError(request);
        TResponse<Integer> response = deviceService.deleteDeviceById(request.getData());
        return response;
    }

    @RequestMapping(value = "/user/update/delete_devices_info", method = RequestMethod.POST)
    public TResponse<Integer> deleteDeviceByIds(@RequestBody() TRequest<int[]> request){
        ParamCheckUtil.handlerError(request);
        TResponse<Integer> response = deviceService.deleteDeviceByIds(request.getData());
        return response;
    }


    @RequestMapping(value = "/user/update/regist_device_info", method = RequestMethod.POST)
    public TResponse<Integer> registDeviceInfo(@RequestBody() TRequest<RegistDeviceReq> request) {
        ParamCheckUtil.handlerError(request);
        TResponse<Integer> response = deviceService.registDevice(request.getData());
        return response;
    }


    @RequestMapping(value = "/user/update/update_device_info", method = RequestMethod.POST)
    public TResponse<Integer> updateDeviceInfo(@RequestBody() TRequest<Device> request) {
        ParamCheckUtil.handlerError(request);
        TResponse<Integer> response = deviceService.updateDevice(request.getData());
        return response;
    }

    @RequestMapping(value = "/admin/query/query_applications_info", method = RequestMethod.POST)
    public TResponse<BaseResp> queryApplications(@RequestBody() TRequest<QueryApplicationsReq> reqTRequest) {
        ParamCheckUtil.handlerError(reqTRequest);
        TResponse<BaseResp> response = checkApplyService.getApplicationsInfoByPageHelper(reqTRequest.getData());
        return response;
    }

    @RequestMapping(value = "/admin/update/delete_applications_info", method = RequestMethod.POST)
    public TResponse<Integer> deleteByIds(@RequestBody() TRequest<int[]> request) {
        ParamCheckUtil.handlerError(request);
        TResponse<Integer> response = checkApplyService.deleteByIds(request.getData());
        return response;
    }


    @RequestMapping(value = "/admin/update/delete_apply_info", method = RequestMethod.POST)
    public TResponse<Integer> deleteById(@RequestBody() TRequest<Integer> request) {
        ParamCheckUtil.handlerError(request);
        TResponse<Integer> response = checkApplyService.deleteById(request.getData());
        return response;
    }


    @RequestMapping(value = "/admin/check/agree_apply_info", method = RequestMethod.POST)
    public TResponse<Integer> agreeApply(@RequestBody() TRequest<CheckReq> request) {
        ParamCheckUtil.handlerError(request);
        TResponse<Integer> response = checkApplyService.agreeApply(request.getData().getUserName(),
                request.getData().getOperationId(),
                request.getData().getApplyType());
        return response;
    }


    @RequestMapping(value = "/admin/check/refused_apply_info", method = RequestMethod.POST)
    public TResponse<Integer> refusedApply(@RequestBody() TRequest<CheckReq> request) {
        ParamCheckUtil.handlerError(request);
        TResponse<Integer> response = checkApplyService.refusedApply(request.getData().getUserName(),
                request.getData().getOperationId(),
                request.getData().getApplyType());
        return response;
    }

    //=================================================================================
    @RequestMapping(value = "/admin/test",method = RequestMethod.POST)
    public String test(@RequestBody() TRequest<String> request){
        return request.getData();
    }



    /**
     * 查询统计信息接口
     * **/
    @Autowired
    DeviceMapper deviceMapper;
    @Autowired
    FarmMapper farmMapper;
    @RequestMapping(value = "/sys/query_stat_info",method = RequestMethod.POST)
    public TResponse<Object> getStatInfo(@RequestBody() TRequest<StatInfoReq> request){
        ParamCheckUtil.handlerError(request);
        int fruitFarmCount = 0;
        int yzFarmCount = 0;
        int nzwFarmCount = 0;
        int onlineFarm = 0;
        int totalFarm = 0;
        int closedFarm = 0;
        int onlineDevice = 0;
        int tempDevice = 0;
        int closedDevice = 0;
        int totalDevice = 0;
        List<Farm> farmStat = farmMapper.getFarmStat(request.getData()==null?null:request.getData().getUserName());
        List<Device> deviceStat = deviceMapper.getDeviceStat(request.getData());
        for(Farm farm : farmStat){
            if(farm.getFarmStatus() == 0){
                onlineFarm+=1;
            }else if(farm.getFarmStatus() == 1){
                closedFarm+=1;
            }
            if(farm.getFarmType() == 1){
                yzFarmCount+=1;
            }else if(farm.getFarmType() == 2){
                fruitFarmCount+=1;
            }else if(farm.getFarmType() == 3){
                nzwFarmCount+=1;
            }
            totalFarm+=1;
        }
        for (Device device:deviceStat){
            if(device.getDeviceStatus() == 10){
                onlineDevice+=1;
            }else if(device.getDeviceStatus() == 0){
                tempDevice+=1;
            }else if(device.getDeviceStatus() == -1){
                closedDevice+=1;
            }
            totalDevice+=1;
        }
        TResponse<Object> response = new TResponse<>();
        StatInfoResp statInfoResp = new StatInfoResp();
        statInfoResp.setClosedDevice(closedDevice);
        statInfoResp.setClosedFarm(closedFarm);
        statInfoResp.setFruitFarmCount(fruitFarmCount);
        statInfoResp.setNzwFarmCount(nzwFarmCount);
        statInfoResp.setOnlineDevice(onlineDevice);
        statInfoResp.setOnlineFarm(onlineFarm);
        statInfoResp.setTempDevice(tempDevice);
        statInfoResp.setTotalDevice(totalDevice);
        statInfoResp.setTotalFarm(totalFarm);
        statInfoResp.setYzFarmCount(yzFarmCount);
        response.setData(statInfoResp);
        return response;
    }

}
