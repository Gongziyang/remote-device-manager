package com.device.manager.main.netty.msg;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/16 14:29
 * @file: WarningMsg
 * @description: 星期二
 */
public class WarningMsg {

    public String warningInfo;

    public int farmId;

    public String warningName;

    public int deviceId;

    public String getWarningInfo() {
        return warningInfo;
    }

    public void setWarningInfo(String warningInfo) {
        this.warningInfo = warningInfo;
    }

    public int getFarmId() {
        return farmId;
    }

    public void setFarmId(int farmId) {
        this.farmId = farmId;
    }

    public String getWarningName() {
        return warningName;
    }

    public void setWarningName(String warningName) {
        this.warningName = warningName;
    }

    public int getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }
}
