package com.device.manager.main.mapper;

import com.device.manager.main.vo.reqeust_entity.InsertCheckApply;
import com.device.manager.main.vo.reqeust_entity.QueryApplicationsReq;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/8 23:32
 * @file: CheckUserApplyMapper
 * @description: 星期一
 */
@Mapper
public interface CheckUserApplyMapper {

    public int insertApply(InsertCheckApply insertCheckApply);

    public List<InsertCheckApply> getApplicationsInfoByPageHelper(QueryApplicationsReq req);

    public int deleteById(int applyId);

    public int deleteByIds(Map<String, Object> idsMap);

    //管理员同意或拒绝申请后，改变审核状态
    public int updateStatus(Map<String, Object> map);

    public int itemSize(String fuzzyName);
}
