package com.device.manager.main.netty.server;

import com.alibaba.fastjson.JSONObject;
import com.device.manager.main.vo.reqeust_entity.CtrlResp;
import com.device.manager.main.netty.msg.*;
import com.device.manager.main.redis.RedisService;
import com.device.manager.main.service.DeviceService;
import com.device.manager.main.service.WarningInfoService;
import com.device.manager.main.vo.entity.DeviceWarning;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelId;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/16 17:14
 * @file: BizHandler
 * @description: 星期二
 */
@Component
public class BizHandler {

    /**
     * 操作数据库 更改设备状态时要用
     **/
    @Autowired
    DeviceService deviceService;

    /**
     * redis订阅-发布 发布警报消息
     **/
    @Autowired
    RedisService redisService;

    /**
     * 操作数据库 插入警报信息
     **/
    @Autowired
    WarningInfoService warningInfoService;

    public static ChannelGroup channels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    private static Logger logger = LoggerFactory.getLogger(BizHandler.class);

    //设备登录时，保存设备id和通道id的映射关系
    public static Map<String, Object> deviceToChannel = new HashMap<>(100);

    //设备离线的时候，通过通道id，找到设备id
    public static Map<Object, String> channelToDevice = new HashMap<>(100);

    //用于阻塞和唤醒线程的锁
    public static Object lock = new Object();

    //消息重发队列 (控制报文) 报文序号-报文
    public static Map<String, Object> resendQue = new HashMap<>(1000);

    //启动定时器
    {
        new Thread(new Runnable() {
            @Override
            public void run() {
                reSend();
                while (true) {
                }
            }
        }).start();

    }


    /**
     * @param baseInf :登录报文
     *                description : 根据登录报文，获取设备id，设备对应的信道channel,将设备的调用 setAttr方法，调用数据库
     *                更更新设备的状态以及设备登录时间。然后向设备发送确认登录报文
     **/
    public void loginHandle(BaseInf<Object> baseInf) {

        DeviceLoginMsg loginMsg = (DeviceLoginMsg) baseInf.getMsg();
        int deviceId = loginMsg.getDeviceId();
        String loginTime = loginMsg.getLoginTime();
        //设置设备状态为已登录
        deviceService.updateState(deviceId, 10);
        //更新设备登录时间
        deviceService.updateLoginTime(deviceId, loginTime);

        //将设备对应的信道保存到map中
        setAttr(loginMsg.getChannel(), deviceId);
        System.out.println("---------------------------------------\n");

        //设置返回命令为4
        baseInf.setCmd((byte) 4);
        RetMsg retMsg = new RetMsg();
        retMsg.setResult(0);
        retMsg.setInfo("服务端已经收到登录消息");
        baseInf.setType((short) 0);
        baseInf.setMsg(retMsg);
        //获取信道
        Channel channelById = getChannelById(deviceId);
        ChannelFuture future = channelById.writeAndFlush(baseInf);
        logger.info("[server-BizHandler-send a pkt]", JSONObject.toJSONString(baseInf));
        future.addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) throws Exception {
                logger.info("BizHandler loginHandle:{}", "成功向客户端返回登录处理结果");
            }
        });
        System.out.println("+++++++++++++++++++++++++++++++++++++++\n");
    }

    /**
     * @param channel :信道
     *                description: 当设备端断开连接时，服务端的channelInactive方法会被触发，此时调用该方法，通过channel在map中
     *                找到对应设备id，移除map中的deviceId - channelId映射，移除channelGroup中的channel,移除
     *                map中的channelid - deviceId映射
     **/
    public int loginOutHandle(Channel channel) {
        ChannelId id = channel.id();
        String deviceId = channelToDevice.get(id);

        //移除  deviceId - channelId 映射关系
        deviceToChannel.remove(deviceId);

        //从channels中移除通到
        channels.remove(id);

        //移除  channelId - deviceId 映射关系
        channelToDevice.remove(id);

        //设置离线状态，修改数据
        deviceService.updateState(Integer.parseInt(deviceId), -1);
        return 0;
    }


    /**
     * @param command  :命令号  0启动  1重启  2暂停  3关闭
     * @param deviceId :设备id
     *                 description: 当浏览器发送控制指令时，先通过设备idz找到对应的信道，然后封装控制报文CtrlMsg,发送报文过后
     *                 将该报文添加到重发队列中，然后阻塞当前线程，当客户端回复之后该线程被唤醒，然后操作数据库更新设备的状态，
     *                 最后返回响应结果给浏览器，如果没有找到信道则返回-1给浏览器。
     **/
    public CtrlResp ctrlHandler(int command, int deviceId) {

        CtrlResp ctrlResp = new CtrlResp();
        Channel channel = getChannelById(deviceId);
        //如果channel已经离线了
        if (channel == null) {
            ctrlResp.setResult(-1);
            ctrlResp.setInfo("设备已离线，请联系农场管理员");
            return ctrlResp;
        }
        //如果信道不活跃，返回-1，前端显示控制失败
        if (!channel.isActive()) {
            ctrlResp.setResult(-1);
            ctrlResp.setInfo("设备状态为止，请联系农场管理员");
            return ctrlResp;
        } else {
            BaseInf<CtrlMsg> baseInf = new BaseInf<>();
            baseInf.setVersion((short) 10);
            baseInf.setCmd((byte) 3);
            CtrlMsg ctrlMsg = new CtrlMsg();
            //0代表启动
            ctrlMsg.setCmd(command);
            baseInf.setMsg(ctrlMsg);
            baseInf.setType((short) 0);
            baseInf.setSeq(30100);
            baseInf.setDeviceId(deviceId);
            //发送控制命令
            channel.writeAndFlush(baseInf);

            logger.info("[server-BizHandler-send a pkt]", JSONObject.toJSONString(baseInf));

            //将报文添加到重发队列中，等待设备端回复
            resendQue.put(baseInf.getSeq() + "", baseInf);
            synchronized (lock) {
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            //重置设备状态
            switch (command) {
                case 0:
                    deviceService.updateState(deviceId, 10);
                    break;
                case 1:
                    deviceService.updateState(deviceId, 10);
                    break;
                case 2:
                    deviceService.updateState(deviceId, 0);
                    break;
                case 3:
                    deviceService.updateState(deviceId, -1);
                    break;
            }

            ctrlResp.setResult(0);
            ctrlResp.setInfo("操作成功");
            System.out.println("+++++++++++++++++++++++++++++++++++++++\n");
            return ctrlResp;
        }
    }

    /**
     * @param baseInf :设备发送的报警报文
     *                description:当收到设备的报警报文后，将该报警消息通过redis推送，以便前端及时获取报警信息，
     *                再将报警记录插入到数据库，方便用户查看
     **/
    public void warningHandle(BaseInf<Object> baseInf) {
        System.out.println("--------收到一条报警记录---------"+JSONObject.toJSONString(baseInf));
        WarningMsg warningMsg = (WarningMsg) baseInf.getMsg();
        warningMsg.setDeviceId(baseInf.getDeviceId());
        String warn = JSONObject.toJSONString(warningMsg);
        //推送一条报警记录
        redisService.sendMessage(warn);
        //向数据库中插入报警记录
        DeviceWarning warning = new DeviceWarning();
        warning.setFarmId(warningMsg.getFarmId());
        warning.setDeviceId(baseInf.getDeviceId());
        warning.setWarningName(warningMsg.getWarningName());
        warning.setWarningInfo(warningMsg.getWarningInfo());
        warningInfoService.insertWarning(warning);
        System.out.println("---------------------------------------\n");

        //回复设备已收到报警消息
        RetMsg retMsg = new RetMsg();
        retMsg.setInfo("服务器已收到报警消息");
        retMsg.setResult(0);
        baseInf.setMsg(retMsg);
        baseInf.setType((short) 0);
        baseInf.setCmd((byte) 4);
        Channel channel = getChannelById(baseInf.getDeviceId());
        ChannelFuture future = channel.writeAndFlush(baseInf);
        logger.info("[server-BizHandler-send a pkt]:{}", JSONObject.toJSONString(baseInf));
        future.addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) throws Exception {
                logger.info("[warning-Handle]", "成功处理一条报警消息，已向设备返回确认信息");
            }
        });
        System.out.println("+++++++++++++++++++++++++++++++++++++++\n");
    }

    /**
     * @param baseInf :设备端回复报文
     *                description: 收到设备端的回复控制报文之后，从重发队列中移除，然后唤醒阻塞的线程
     **/
    public void replyHandle(BaseInf<Object> baseInf) {
        //获取返回报文
        RetMsg retMsg = (RetMsg) baseInf.getMsg();

        //从重发队列中移除
        resendQue.remove(baseInf.getSeq() + "");

        //唤醒阻塞的线程
        notification();
        logger.info("收到设备的控制回复报文,准备响应浏览器:{}", JSONObject.toJSONString(baseInf));
    }
    /**======================================================================================**/

    /**
     * @param channel:信到
     * @param deviceId:设备id description: 当设备登陆时，需要根据设备id将设备对应得channel.id()存放到map中，
     *                      还需要将 channel-deviceId得映射放到map中，设备离线得时候可以根据信道找到设备id
     *                      最后将channel添加到channelGroup中
     **/
    public void setAttr(Channel channel, int deviceId) {
        //将该信道添加到ChannelGourp中
        channels.add(channel);

        //添加  deviceId - channelId 映射关系
        deviceToChannel.put(deviceId + "", channel.id());

        //添加  channelId -deviceId 映射关系
        channelToDevice.put(channel.id(), deviceId + "");

    }

    /**
     * @param deviceId :设备id
     *                 description: 通过设备id,找到channelId,通过channelGroup.find()方法找到对应得信道channel
     *                 如果没有找到则返回空
     **/
    public static Channel getChannelById(int deviceId) {
        ChannelId channelId = (ChannelId) deviceToChannel.get(deviceId + "");
        if (channelId == null) {
            return null;
        }
        //通过ChannleId得到该信道信息
        Channel channel = channels.find(channelId);
        return channel;
    }


    /**
     * description: 当服务端收到回复报文时，唤醒正在阻塞的线程，响应浏览器
     **/
    public void notification() {
        //唤醒这个对象上的所有的线程
        synchronized (lock) {
            lock.notify();
        }
    }


    /**
     * 启动定时器，轮询重发队列，如果有需要重发的报文，通过报文中的设备ID，找到设备id，通过设备id找到channel
     * 然后通过channel重新发送报文
     **/
    public void reSend() {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                logger.info("定时器启动：{}", "轮询重发队列中...");
                if (resendQue != null) {
                    //如果重发队列中有需要重发的报文
                    if (resendQue.size() > 0) {
                        Set<String> key = resendQue.keySet();
                        Iterator<String> iteratorKey = key.iterator();
                        String seq;
                        BaseInf<CtrlMsg> msg;
                        while (iteratorKey.hasNext()) {
                            seq = iteratorKey.next();
                            msg = (BaseInf<CtrlMsg>) resendQue.get(seq);
                            int deviceId = msg.getDeviceId();
                            //找到对应的信道
                            Channel channel = getChannelById(deviceId);
                            if (channel == null) {
                                logger.info("重发报文失败：{}", "无效的channel,该设备已离线");
                                continue;
                            }
                            channel.writeAndFlush(msg);
                            logger.info("定时器完成一条消息重发：{}", JSONObject.toJSONString(msg));
                            System.out.println("+++++++++++++++++++++++++++++++++++++++\n");
                        }
                    }
                }
            }
        }, 1000 * 5, 20000);
    }
}
