package com.device.manager.main.mapper.sys_user;

import com.device.manager.main.vo.sys_user.SysPermission;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/21 13:17
 * @file: SysPermissionMapper
 * @description: 星期日
 */
@Mapper
public interface SysPermissionMapper {

    /**
     * @param userName : 用户名
     *                 description : 通过用户名拿到用户的权限集合
     **/
    List<SysPermission> getByUser(String userName);
}
