package com.device.manager.main.mapper;

import com.device.manager.main.vo.entity.Device;
import com.device.manager.main.vo.reqeust_entity.QueryDeviceReq;
import com.device.manager.main.vo.reqeust_entity.RegistDeviceReq;
import com.device.manager.main.vo.reqeust_entity.StatInfoReq;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/9 13:55
 * @file: DeviceMapper
 * @description: 星期二
 */
@Mapper
public interface DeviceMapper {


    public int updateDevice(Device device);

    public int deleteDevicesByUserName(String userName);

    public int deleteDevicesByUserNames(Map<String, Object> userNames);

    public int deleteDevicesByFarmId(int farmId);

    public int deleteDevicesByFarmIds(Map<String, Object> farmIds);

    public int deleteDeviceById(int deviceId);

    public int deleteDevicesByIds(Map<String, Object> deviceIds);

    public List<Device> getDevicesByPageHelper(QueryDeviceReq req);

    public int itemSize(Map<String, Object> info);

    /**
     * 普通用户所需特殊接口：注册农场
     **/
    public int registDevice(RegistDeviceReq deviceReq);

    /**
     * 管理员所用接口
     **/
    public int insertDevice(Device device);

    public int updateState(Map<String, Object> map);

    public int updateLoginTime(Map<String, Object> map);

    List<Integer> getDeviceIdsByUserName(String userName);

    List<Device> getDeviceStat(StatInfoReq req);

    List<Integer> getDeviceIds();
}
