package com.device.manager.main.netty.test.client1;

import com.device.manager.main.netty.msg.BaseInf;
import com.device.manager.main.netty.msg.CtrlMsg;
import com.device.manager.main.netty.msg.HeartBeatMsg;
import com.device.manager.main.netty.msg.RetMsg;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.util.Attribute;
import io.netty.util.AttributeKey;

import java.util.Date;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/15 23:38
 * @file: ClientHandler
 * @description: 星期一
 */
public class ClientHandler extends SimpleChannelInboundHandler<BaseInf<Object>> {

    private BizHandler bizHandler = new BizHandler();

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, BaseInf<Object> baseInf) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        System.out.println("[receiived-from-server]" + objectMapper.writeValueAsString(baseInf));
        Object msg = baseInf.getMsg();
        if (msg instanceof RetMsg) {
            bizHandler.relpyHandle(baseInf);
        }
        if (msg instanceof CtrlMsg) {
            bizHandler.ctrlHandle(baseInf, channelHandlerContext.channel());
        }
    }

    protected ClientHandler() {
        super();
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        AttributeKey<Object> attributeKey = AttributeKey.valueOf("id");
        Attribute<Object> attr = ctx.channel().attr(attributeKey);
        attr.setIfAbsent("netty他妈之拉稀");
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        super.channelInactive(ctx);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        super.exceptionCaught(ctx, cause);
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        IdleState state = ((IdleStateEvent) evt).state();
        //如果是读取超时
        if (state.equals(IdleState.READER_IDLE)) {
            BaseInf<HeartBeatMsg> baseInf = new BaseInf<>();
            HeartBeatMsg heartBeatMsg = new HeartBeatMsg();
            Date date = new Date(System.currentTimeMillis());
            heartBeatMsg.setHeartBeatTime(date.toString());
            System.out.println(heartBeatMsg.getHeartBeatTime());
            baseInf.setCmd((byte) 5);
            baseInf.setMsg(heartBeatMsg);
            ctx.writeAndFlush(baseInf);
            System.out.println("客户端发送心跳");
        }
    }
}
