package com.device.manager.main.controller.controller;

import com.device.manager.main.netty.msg.WarningMsg;
import com.device.manager.main.netty.server.BizHandler;
import com.device.manager.main.redis.MessageReciverSupport;
import com.device.manager.main.service.DeviceService;
import com.device.manager.main.service.UserService;
import com.device.manager.main.vo.reqeust_entity.CtrlReq;
import com.device.manager.main.vo.reqeust_entity.CtrlResp;
import com.device.manager.main.vo.msg_standard.TRequest;
import com.device.manager.main.vo.msg_standard.TResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.ArrayUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/16 16:31
 * @file: ControlDeviceController
 * @description: 星期二
 */
@CrossOrigin
@RestController
@Api(tags = "设备控制及设备报警接口[启动、重启、暂停、关闭]")
public class ControlDeviceController {

    @Autowired
    BizHandler bizHandler;

    @Autowired
    MessageReciverSupport messageReciverSupport;

    @Autowired
    DeviceService deviceService;

    @Autowired
    UserService userService;

    @RequestMapping(value = "/ctrl/ctrl_device", method = RequestMethod.POST)
    public TResponse<CtrlResp> ctrlDevice(@RequestBody() TRequest<CtrlReq> reqTRequest) {
        CtrlResp resp = bizHandler.ctrlHandler(reqTRequest.getData().getCommand(), reqTRequest.getData().getDeviceId());
        TResponse<CtrlResp> response = new TResponse<>();
        response.setData(resp);
        return response;
    }


    @RequestMapping(value = "/monitor/monitor_warning", method = RequestMethod.POST)
    public TResponse<WarningMsg> monitorWarn(@RequestBody() TRequest<String> request) {
        Set<String> userRolesSet = userService.getUserRolesSet(request.getData());
        List<Integer> deviceIds = null;
        if(userRolesSet.contains("admin")){
             deviceIds = deviceService.getDeviceIds();
        }else {
            deviceIds = deviceService.getDeviceIdsByUserName(request.getData());
        }
        System.out.println("[拥有的设备id]"+ ArrayUtils.toString(deviceIds));
        WarningMsg warn = messageReciverSupport.getWarnByUserName(deviceIds);
        TResponse<WarningMsg> response = new TResponse<>();
        response.setData(warn);
        return response;
    }
}
