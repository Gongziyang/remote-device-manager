package com.device.manager.main.advice;

import com.device.manager.main.constant.CommonConstant;
import com.device.manager.main.constant.Constant;
import org.apache.shiro.authz.UnauthenticatedException;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/9 0:19
 * @file: ExceptionHandler
 * @description: 星期二
 * @ControllerAdvice 能捕获到所有SpringMVC控制器中的异常，但是捕获不到非控制器中的异常
 */
//@ControllerAdvice
public class ExceptionHandler {


    @org.springframework.web.bind.annotation.ExceptionHandler(Exception.class)
    @ResponseBody
    public Map<String, Object> errorHandle(Exception e) {

        Map<String, Object> result = new HashMap<>(10);
        //如果身份验证失败
        if (e instanceof UnauthenticatedException) {
            result.put("ret", CommonConstant.AUTHENTICATION_FAILED);
            result.put("msg", "请重新登录");
        } else if (e instanceof UnauthorizedException) {
            result.put("ret", CommonConstant.NO_PERMISSION);
            result.put("msg", "无权限访问，请联系管理员");
        } else {
            result.put("ret", CommonConstant.INTERNEL_ERROR);
            result.put("msg", e.toString());
        }
        result.put("Data", null);
        result.put("TimeStamp", new SimpleDateFormat("yyyMMddHHmmss")
                .format(new Date(System.currentTimeMillis())));
        return result;
    }

}
