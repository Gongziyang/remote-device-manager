package com.device.manager.main.vo.reqeust_entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/8 20:59
 * @file: QueryFarmReq
 * @description: 星期一
 */
@ApiModel(description = "包含查询农场信息的请求参数")
public class QueryFarmReq extends BaseReq {

    //搜索筛选条件
    @ApiModelProperty(name = "farmName", value = "农场名称", dataType = "Integer", example = "十陵养猪场")
    private String farmName = null;
    @ApiModelProperty(name = "registTime", value = "注册时间", dataType = "String", example = "2020-09-03")
    private String registTime = null;
    @ApiModelProperty(name = "farmNumber", value = "农场编号", dataType = "String", example = "001")
    private String farmNumber = null;
    @ApiModelProperty(name = "userName", value = "用户名", dataType = "String", example = "yyf")
    private String userName = null;
    @ApiModelProperty(name = "type", value = "农场类型", dataType = "Integer", example = "1")
    private int type = -1;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public int getPageNo() {
        return pageNo;
    }

    @Override
    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    @Override
    public int getPageSize() {
        return pageSize;
    }

    @Override
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getFarmName() {
        return farmName;
    }

    public void setFarmName(String farmName) {
        this.farmName = farmName;
    }

    public String getRegistTime() {
        return registTime;
    }

    public void setRegistTime(String registTime) {
        this.registTime = registTime;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getFarmNumber() {
        return farmNumber;
    }

    public void setFarmNumber(String farmNumber) {
        this.farmNumber = farmNumber;
    }
}
