package com.device.manager.main.vo.reqeust_entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/11 17:29
 * @file: QueryApplicationsReq
 * @description: 星期四
 */
@ApiModel(description = "包含查询用户申请信息所需要的参数")
public class QueryApplicationsReq extends BaseReq {


    //筛选条件
    @ApiModelProperty(name = "applyTime", value = "申请时间", dataType = "String", example = "2020-09-13")
    private String applyTime;
    @ApiModelProperty(name = "status", value = "审核状态", dataType = "Integer", example = "0")
    private int status = -1;
    @ApiModelProperty(name = "applyType", value = "申请类型", dataType = "Integer", example = "1")
    private int applyType = -1;
    private String fuzzyName = null;

    public String getFuzzyName() {
        return fuzzyName;
    }

    public void setFuzzyName(String fuzzyName) {
        this.fuzzyName = fuzzyName;
    }

    @Override
    public int getPageNo() {
        return pageNo;
    }

    @Override
    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    @Override
    public int getPageSize() {
        return pageSize;
    }

    @Override
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(String applyTime) {
        this.applyTime = applyTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getApplyType() {
        return applyType;
    }

    public void setApplyType(int applyType) {
        this.applyType = applyType;
    }
}
