package com.device.manager.main.vo.reqeust_entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/11 15:04
 * @file: QueryUserReq
 * @description: 星期四
 */
@ApiModel(description = "包含管理员查询用户信息时所需要的参数信息")
public class QueryUserReq extends BaseReq {


    //筛选条件
    @ApiModelProperty(name = "userName", value = "用户名", dataType = "String", example = "yyf")
    private String userName;
    @ApiModelProperty(name = "frezzeState", value = "账户状态", dataType = "Integer", example = "0[正常]")
    private int frezzeState = -1;

    @Override
    public int getPageNo() {
        return pageNo;
    }

    @Override
    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    @Override
    public int getPageSize() {
        return pageSize;
    }

    @Override
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getFrezzeState() {
        return frezzeState;
    }

    public void setFrezzeState(int frezzeState) {
        this.frezzeState = frezzeState;
    }
}
