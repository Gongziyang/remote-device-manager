package com.device.manager.main.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/9 20:57
 * @file: FarmUserRelationMapper
 * @description: 星期二
 */
@Mapper
public interface FarmUserRelationMapper {
    public int delByFarmIds(Map<String, Object> farmIdsMap);

    public int delByFarmId(int farmId);

    //通过用户ID来找出该用户所对应的农场id
    public Integer[] getFarmIdsByUserId(int userId);

    public Integer[] getFarmIdsByUserIds(Map<String, Object> userIds);
}
