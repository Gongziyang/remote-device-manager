package com.device.manager.main.netty;

import com.alibaba.fastjson.JSONObject;
import com.device.manager.main.netty.msg.*;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/16 19:57
 * @file: Decoder
 * @description: 星期二
 */
public class Decoder extends ByteToMessageDecoder {

    public static Logger logger = LoggerFactory.getLogger(Decoder.class);

    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext,
                          ByteBuf byteBuf, List<Object> list) throws Exception {
        System.out.println("---------------------------------------");
        logger.info("Decoder:{}", "准备解码...");
        //读取version 2字节
        short version = byteBuf.readShort();
        //读取报文长度  2字节
        short len = byteBuf.readShort();
        //读取报文序号  4字节
        int seq = byteBuf.readInt();
        //读取类型  2字节
        short type = byteBuf.readShort();
        //读取cmd  1个字节
        byte cmd = byteBuf.readByte();
        //读取deviceId 4个字节
        int deviceId = byteBuf.readInt();

        byte[] bytes = null;
        JSONObject jsonObject = null;
        BaseInf<Object> baseInf = null;

        if (type == Constant.PLATFORM_TO_DEVICE) {
            switch (cmd) {
                case Constant.CONTROL_MSG:
                    //设备解析平台发送的控制报文
                    logger.info("Decoder:{}", "该报文是平台控制报文，解码中...");
                    bytes = new byte[len - Constant.HEADER_SIZE];
                    System.out.println("[decoder-switch-0-case-3]" + bytes.length);
                    byteBuf.readBytes(bytes);
                    String msg = new String(bytes);
                    jsonObject = JSONObject.parseObject(msg);
                    CtrlMsg ctrlMsg = jsonObject.toJavaObject(CtrlMsg.class);
                    baseInf = setInfo(version, len, seq, type, cmd, deviceId, ctrlMsg, byteBuf);
                    //添加到队列中，等待下一步业务处理
                    list.add(baseInf);
                    break;
                case Constant.REPLY_MSG:
                    //解析返回结果报文
                    logger.info("Decoder:{}", "该报文是响应报文，解码中...");
                    bytes = new byte[len - Constant.HEADER_SIZE];
                    byteBuf.readBytes(bytes);
                    jsonObject = JSONObject.parseObject(new String(bytes));
                    RetMsg retMsg = jsonObject.toJavaObject(RetMsg.class);
                    baseInf = setInfo(version, len, seq, type, cmd, deviceId, retMsg, byteBuf);
                    //添加到队列中，等待下一步业务处理
                    list.add(baseInf);
                    break;
                default:
                    break;
            }
        }

        if (type == Constant.DEVICE_TO_PLATFORM) {
            switch (cmd) {
                case Constant.LOGIN_MSG:
                    logger.info("Decoder:{}", "该报文是登录报文，处理中...");
                    //平台解析设备登录报文
                    bytes = new byte[len - Constant.HEADER_SIZE];
                    byteBuf.readBytes(bytes);
                    String msg = new String(bytes);
                    jsonObject = JSONObject.parseObject(msg);
                    DeviceLoginMsg loginMsg = jsonObject.toJavaObject(DeviceLoginMsg.class);
                    loginMsg.setChannel(channelHandlerContext.channel());
                    baseInf = setInfo(version, len, seq, type, cmd, deviceId, loginMsg, byteBuf);
                    //添加到队列中，等待下一步业务处理
                    list.add(baseInf);
                    break;
                case Constant.WARN_MSG:
                    //平台解析设备报警的报文
                    logger.info("Decoder:{}", "该报文是设备报警报文，解码中...");
                    bytes = new byte[len - Constant.HEADER_SIZE];
                    byteBuf.readBytes(bytes);
                    jsonObject = JSONObject.parseObject(new String(bytes));
                    WarningMsg warningMsg = jsonObject.toJavaObject(WarningMsg.class);
                    baseInf = setInfo(version, len, seq, type, cmd, deviceId, warningMsg, byteBuf);
                    //添加到队列中，等待下一步业务处理
                    list.add(baseInf);
                    break;
                case Constant.REPLY_MSG:
                    //解析返回结果报文
                    logger.info("Decoder:{}", "该报文是响应报文，解码中...");
                    bytes = new byte[len - Constant.HEADER_SIZE];
                    byteBuf.readBytes(bytes);
                    jsonObject = JSONObject.parseObject(new String(bytes));
                    RetMsg retMsg = jsonObject.toJavaObject(RetMsg.class);
                    baseInf = setInfo(version, len, seq, type, cmd, deviceId, retMsg, byteBuf);
                    //添加到队列中，等待下一步业务处理
                    list.add(baseInf);
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
    }

    public BaseInf<Object> setInfo(short version, short len,
                                   int seq, short type, byte cmd,
                                   int deviceId, Object msg, ByteBuf byteBuf) {
        BaseInf<Object> baseInf = new BaseInf<>();
        baseInf.setVersion(version);
        baseInf.setLength(len);
        baseInf.setSeq(seq);
        baseInf.setType(type);
        baseInf.setCmd(cmd);
        baseInf.setDeviceId(deviceId);
        baseInf.setMsg(msg);
        //读取crc校验码
        short crc = byteBuf.readShort();
        baseInf.setCrc(crc);
        logger.info("Decoder:{}", "解析完成，成功解析一个响应报文，提交到业务层处理");
        return baseInf;

    }

}
