package com.device.manager.main.vo.reqeust_entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/11 23:45
 * @file: CheckReq
 * @description: 星期四
 */
@ApiModel(description = "包含管理员[同意/决绝]用户申请的时候提交的请求参数")
public class CheckReq {

    @ApiModelProperty(name = "userName", value = "用户名", dataType = "String", example = "1")
    private String userName;
    @ApiModelProperty(name = "operationId", value = "操作id", dataType = "Integer", example = "1")
    private int operationId;
    @ApiModelProperty(name = "applyType", value = "申请时间", dataType = "Integer", example = "1")
    private int applyType;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getOperationId() {
        return operationId;
    }

    public void setOperationId(int operationId) {
        this.operationId = operationId;
    }

    public int getApplyType() {
        return applyType;
    }

    public void setApplyType(int applyType) {
        this.applyType = applyType;
    }
}
