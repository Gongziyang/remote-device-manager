package com.device.manager.main.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.device.manager.main.constant.Constant;
import com.device.manager.main.mapper.CheckUserApplyMapper;
import com.device.manager.main.service.*;
import com.device.manager.main.utils.custom.ResponseUtils;
import com.device.manager.main.vo.entity.Device;
import com.device.manager.main.vo.entity.Farm;
import com.device.manager.main.vo.msg_standard.TResponse;
import com.device.manager.main.vo.reqeust_entity.BaseResp;
import com.device.manager.main.vo.reqeust_entity.InsertCheckApply;
import com.device.manager.main.vo.reqeust_entity.QueryApplicationsReq;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/9 0:09
 * @file: CheckApplyServiceImpl
 * @description: 星期二
 */
@Service
public class CheckApplyServiceImpl implements CheckApplyService {

    @Autowired
    CheckUserApplyMapper checkUserApplyMapper;

    @Autowired
    RegistFarmRecordService registFarmRecordService;

    @Autowired
    RegistDeviceRecordService registDeviceRecordService;

    @Autowired
    FarmService farmService;

    @Autowired
    DeviceService deviceService;

    @Autowired
    UserOperationService userOperationService;

    @Override
    public int insertApply(InsertCheckApply insertCheckApply) {
        return checkUserApplyMapper.insertApply(insertCheckApply);
    }

    @Override
    public TResponse<BaseResp> getApplicationsInfoByPageHelper(QueryApplicationsReq req) {
        int pageNo = req.getPageNo();
        int pageSize = req.getPageSize();
        if(pageNo == 0){
            pageNo = 1;
        }
        if(pageSize == 0){
            pageSize = 5;
        }
        PageHelper.startPage(pageNo, pageSize);
        TResponse<BaseResp> response = new TResponse<>();
        List<InsertCheckApply> applications = checkUserApplyMapper.getApplicationsInfoByPageHelper(req);
        int itemSize = checkUserApplyMapper.itemSize(req.getFuzzyName());
        int pageCount = itemSize % req.getPageSize() == 0 ? itemSize / req.getPageSize() : (itemSize / req.getPageSize() + 1);
        if (applications == null || applications.size() == 0) {
            ResponseUtils.set(response, -1, "暂无更多信息", null);
        } else {
            BaseResp applicationsResp = new BaseResp();
            applicationsResp.setList(applications);
            applicationsResp.setItemSize(itemSize);
            applicationsResp.setPageCount(pageCount);
            applicationsResp.setPageNo(req.getPageNo());
            ResponseUtils.set(response, 0, "查询成功，共查询到：" + itemSize + "条记录", applicationsResp);
        }
        response.setTimeStamp(getTimestamp());
        return response;
    }

    @Override
    public TResponse<Integer> deleteById(int applyId) {
        int affects = checkUserApplyMapper.deleteById(applyId);
        TResponse<Integer> response = new TResponse<>();
        if (affects == 0) {
            ResponseUtils.set(response, -1, "操作失败", -1);
        } else {
            ResponseUtils.set(response, 0, "操作成功", 0);
        }
        response.setTimeStamp(getTimestamp());
        return response;
    }

    @Override
    public TResponse<Integer> deleteByIds(int[] ids) {
        Map<String, Object> idsMap = new HashMap<>(16);
        TResponse<Integer> response = new TResponse<>();
        idsMap.put("ids", ids);
        int affects = checkUserApplyMapper.deleteByIds(idsMap);
        if (affects == 0) {
            ResponseUtils.set(response, -1, "操作失败", -1);
        } else {
            ResponseUtils.set(response, 0, "操作成功", affects);
        }
        response.setTimeStamp(getTimestamp());
        return response;
    }


    /**
     * 通过判断applyType来判断是注册设备还是注册农场
     **/
    @Override
    public TResponse<Integer> agreeApply(String userName, int operationId, int applyType) {
        TResponse<Integer> response = new TResponse<>();
        //注册农场
        if (applyType == Constant.REGIST_FARM) {
            Farm farm = registFarmRecordService.getFarmByOptionIdAndUserName(userName, operationId);
            int affects = farmService.insertFarm(farm);
            if (affects == 0) {
                ResponseUtils.set(response, -1, "操作失败", -1);
            } else {
                ResponseUtils.set(response, 0, "操作成功", affects);
                //删除注册农场表中该记录
                registFarmRecordService.deleteByOptionIdAndUserName(userName, operationId);
                userOperationService.updateStatus(userName, operationId, 0);
                //将管理员审核表中的记录设置为已通过
                updateStatus(userName, operationId, 0);
            }
        } else if (applyType == Constant.REGIST_DEVICE) {
            Device device = registDeviceRecordService.getDeviceByOptionIdAndUserName(userName, operationId);
            System.out.println("[------------从记录表中获取的设备信息------------]"+ JSONObject.toJSONString(device));
            int affects = deviceService.insertDevice(device);
            if (affects == 0) {
                ResponseUtils.set(response, -1, "操作失败", -1);
            } else {
                ResponseUtils.set(response, 0, "操作成功", affects);
                //删除注册农场表中该记录
                registDeviceRecordService.deleteByOptionIdAndUserName(userName, operationId);
                userOperationService.updateStatus(userName, operationId, 0);
                //设置用户操作记录的审核状态为通过
                updateStatus(userName, operationId, 0);
            }
        }
        response.setTimeStamp(getTimestamp());
        return response;
    }

    @Override
    public TResponse<Integer> refusedApply(String userName, int operationId, int applyType) {
        TResponse<Integer> response = new TResponse<>();
        int affects0 = -1, affrcts1 = -1;
        if (applyType == Constant.REGIST_FARM) {
            affects0 = registFarmRecordService.deleteByOptionIdAndUserName(userName, operationId);
            affrcts1 = userOperationService.updateStatus(userName, operationId, 0);
            updateStatus(userName, operationId, 0);
        } else if (applyType == Constant.REGIST_DEVICE) {
            affects0 = registDeviceRecordService.deleteByOptionIdAndUserName(userName, operationId);
            affrcts1 = userOperationService.updateStatus(userName, operationId, -1);
            updateStatus(userName, operationId, -1);
        }
        if (affects0 + affrcts1 <= 2) {

            ResponseUtils.set(response, 0, "操作失败", -1);

        } else {

            ResponseUtils.set(response, 0, "操作成功", affects0 + affrcts1);
        }
        response.setTimeStamp(getTimestamp());
        return response;
    }

    @Override
    public int updateStatus(String userName, int operationId, int status) {
        Map<String, Object> map = new HashMap<>();
        map.put("userName", userName);
        map.put("operationId", operationId);
        map.put("status", status);
        return checkUserApplyMapper.updateStatus(map);
    }

    private String getTimestamp() {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
        String timeStamp = format.format(System.currentTimeMillis());
        return timeStamp;
    }
}
