package com.device.manager.main.vo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/8 16:22
 * @file: Device
 * @description: 星期一
 */
public class Device {

    private int deviceId; //设备ID

    private String deviceNumber; //设备编号，必填字段

    private String deviceName; //设别名称，必填字段

    private int deviceType; //设备类型，必填字段

    private float deviceLng; //设备经度，必填字段

    private float deviceLat; //设备纬度，必填字段

    private int deviceStatus; //设备状态，必填字段

    private String deviceInfo; //设备说明

    private String registTime;

    private int farmId;

    private String warningInfo;

    public int getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public int getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(int deviceType) {
        this.deviceType = deviceType;
    }

    public float getDeviceLng() {
        return deviceLng;
    }

    public void setDeviceLng(float deviceLng) {
        this.deviceLng = deviceLng;
    }

    public float getDeviceLat() {
        return deviceLat;
    }

    public void setDeviceLat(float deviceLat) {
        this.deviceLat = deviceLat;
    }

    public int getDeviceStatus() {
        return deviceStatus;
    }

    public void setDeviceStatus(int deviceStatus) {
        this.deviceStatus = deviceStatus;
    }

    public String getDeviceInfo() {
        return deviceInfo;
    }

    public String getRegistTime() {
        return registTime;
    }

    public void setRegistTime(String registTime) {
        this.registTime = registTime;
    }

    public void setDeviceInfo(String deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public int getFarmId() {
        return farmId;
    }

    public void setFarmId(int farmId) {
        this.farmId = farmId;
    }

    public String getWarningInfo() {
        return warningInfo;
    }

    public void setWarningInfo(String warningInfo) {
        this.warningInfo = warningInfo;
    }
}
