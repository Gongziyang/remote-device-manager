package com.device.manager.main.vo.reqeust_entity;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/21 15:25
 * @file: BaseReq
 * @description: 星期日
 */
@ApiModel(description = "前台请求基类")
public class BaseReq {

    @ApiModelProperty(name = "pageNo", value = "当前页码", dataType = "Integer", example = "1")
    public int pageNo;
    @ApiModelProperty(name = "pageSize", value = "页面大小", dataType = "Integer", example = "10")
    public int pageSize;

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
