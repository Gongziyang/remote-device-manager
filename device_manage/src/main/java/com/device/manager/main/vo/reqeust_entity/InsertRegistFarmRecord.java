package com.device.manager.main.vo.reqeust_entity;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/8 23:51
 * @file: InsertRegistFarmRecord
 * @description: 星期一
 */
public class InsertRegistFarmRecord {
    private String userName;

    private int operationId;

    private String farmNumber;

    private String farmName;

    private int farmType;

    private float farmLng;

    private float farmLat;

    private String farmInfo;

    private int farmStatus;

    private String registTime;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getOperationId() {
        return operationId;
    }

    public void setOperationId(int operationId) {
        this.operationId = operationId;
    }

    public String getFarmNumber() {
        return farmNumber;
    }

    public void setFarmNumber(String farmNumber) {
        this.farmNumber = farmNumber;
    }

    public String getFarmName() {
        return farmName;
    }

    public void setFarmName(String farmName) {
        this.farmName = farmName;
    }

    public int getFarmType() {
        return farmType;
    }

    public void setFarmType(int farmType) {
        this.farmType = farmType;
    }

    public float getFarmLng() {
        return farmLng;
    }

    public void setFarmLng(float farmLng) {
        this.farmLng = farmLng;
    }

    public float getFarmLat() {
        return farmLat;
    }

    public void setFarmLat(float farmLat) {
        this.farmLat = farmLat;
    }

    public String getFarmInfo() {
        return farmInfo;
    }

    public void setFarmInfo(String farmInfo) {
        this.farmInfo = farmInfo;
    }


    public int getFarmStatus() {
        return farmStatus;
    }

    public void setFarmStatus(int farmStatus) {
        this.farmStatus = farmStatus;
    }

    public String getRegistTime() {
        return registTime;
    }

    public void setRegistTime(String registTime) {
        this.registTime = registTime;
    }
}
