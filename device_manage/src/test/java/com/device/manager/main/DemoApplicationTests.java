package com.device.manager.main;

import com.alibaba.fastjson.JSONObject;
import com.device.manager.main.service.FarmService;
import com.device.manager.main.vo.entity.Farm;
import com.device.manager.main.vo.msg_standard.TResponse;
import com.device.manager.main.vo.reqeust_entity.BaseResp;
import com.device.manager.main.vo.reqeust_entity.QueryFarmReq;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
class DemoApplicationTests {


    @Autowired
    FarmService farmService;

    @Test
    void contextLoads() {
        QueryFarmReq farmReq = new QueryFarmReq();
        farmReq.setPageNo(1);
        farmReq.setPageSize(10);
        TResponse<BaseResp> farmByPageHelper = farmService.getFarmByPageHelper(farmReq);
        List<Farm> list = (List<Farm>) farmByPageHelper.getData().getList();
        System.out.println(JSONObject.toJSONString(list));
    }

}
